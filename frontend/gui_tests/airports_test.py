from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager
import unittest

URL = "https://dev.parkdex.me/airports"

class AirportsTest(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_experimental_option("detach", True)
        service = Service(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get(URL)
  
    def tearDown(self) -> None:
        self.driver.quit()

    def testAirportRows(self):
        """
        Make sure airport rows can be clicked on.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.CLASS_NAME, "models-page-content tr td"))
            )
        except Exception as e:
            self.fail("Airport rows don't display: " + str(e))
        elem = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tbody tr")
        elem.click()
        self.assertEqual(self.driver.current_url, URL + "/14721")

    def testNextButton(self):
        """
        Make sure the next button works.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "next-button"))
            )
        except Exception as e:
            self.fail("Next button not found: " + str(e))
        firstAiport = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tr td").text
        next = self.driver.find_element(By.ID, "next-button")
        self.driver.execute_script("arguments[0].click();", next)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.CSS_SELECTOR, ".models-page-content tr td"))
            )
        except Exception as e:
            self.fail("Airport rows not loading: " + str(e))
        secondAirport = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tr td").text
        self.assertNotEqual(firstAiport, secondAirport)
