from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager
import unittest

URL = "https://dev.parkdex.me/hotels"

class HotelsTest(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_experimental_option("detach", True)
        service = Service(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get(URL)
  
    def tearDown(self) -> None:
        self.driver.quit()

    def testHotelRow(self):
        """
        Make sure hotel rows can be clicked on.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.CLASS_NAME, "models-page-content tr td"))
            )
        except Exception as e:
            self.fail("Hotel rows don't display: " + str(e))
        elem = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tbody tr")
        self.driver.execute_script("arguments[0].click();", elem)
        self.assertEqual(self.driver.current_url, URL + "/38944")

    def testNextButton(self):
        """
        Make sure the next button works.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "next-button"))
            )
        except Exception as e:
            self.fail("Next button not found: " + str(e))
        firstHotel = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tr td").text
        next = self.driver.find_element(By.ID, "next-button")
        self.driver.execute_script("arguments[0].click();", next)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.CSS_SELECTOR, ".models-page-content tr td"))
            )
        except Exception as e:
            self.fail("Hotel rows not loading: " + str(e))
        secondHotel = self.driver.find_element(By.CSS_SELECTOR, ".models-page-content tr td").text
        self.assertNotEqual(firstHotel, secondHotel)
    
    def testBrandsFilter(self):
        """
        Make sure hotel brands filter works.
        """
        # Wait for the hotel rows to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-38944"))
            )
        except Exception as e:
            self.fail("Hotel rows doesn't load: " + str(e))
        select = Select(self.driver.find_element(By.ID, "Brands-dropdown"))
        select.select_by_visible_text("Four Seasons")
        apply = self.driver.find_element(By.ID, "apply")
        self.driver.execute_script("arguments[0].click();", apply)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-1480"))
            )
        except Exception as e:
            self.fail("Hotel brands filter doesn't work: " + str(e))

    def testSearch(self):
        """
        Make sure searching works.
        """
        # Wait for the hotel rows to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-38944"))
            )
        except Exception as e:
            self.fail("Hotel rows doesn't load: " + str(e))
        self.driver.find_element(By.ID, "search").send_keys("Adelphi Hotel")
        self.driver.find_element(By.CLASS_NAME, "search-button").click()
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-45520"))
            )
        except Exception as e:
            self.fail("Searching hotels doesn't work: " + str(e))

    def testSort(self):
        """
        Make sure sorting works.
        """
        # Wait for the hotel rows to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-38944"))
            )
        except Exception as e:
            self.fail("Hotel rows doesn't load: " + str(e))
        select = Select(self.driver.find_element(By.ID, "Sort-dropdown"))
        select.select_by_visible_text("Name (Z-A)")
        apply = self.driver.find_element(By.ID, "apply")
        self.driver.execute_script("arguments[0].click();", apply)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "hotel-card-12174"))
            )
        except Exception as e:
            self.fail("Sorting hotels doesn't work: " + str(e))