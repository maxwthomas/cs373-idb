from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager
import unittest

URL = "https://dev.parkdex.me/parks"

class ParksTest(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_experimental_option("detach", True)
        service = Service(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get(URL)
  
    def tearDown(self) -> None:
        self.driver.quit()

    def testParkCard(self):
        """
        Make sure park cards can be clicked on.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-0"))
            )
        except Exception as e:
            self.fail("Park cards don't display: " + str(e))
        elem = self.driver.find_element(By.ID, "park-card-0")
        self.driver.execute_script("arguments[0].click();", elem)
        self.assertEqual(self.driver.current_url, URL + "/0")

    def testNextButton(self):
        """
        Make sure the next button works.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "next-button"))
            )
        except Exception as e:
            self.fail("Next button not found: " + str(e))
        elem = self.driver.find_element(By.ID, "next-button")
        self.driver.execute_script("arguments[0].click();", elem)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-20"))
            )
        except Exception as e:
            self.fail("Next button doesn't work: " + str(e))

    def testStateFilter(self):
        """
        Make sure state filter works.
        """
        # Wait for the park cards to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-1"))
            )
        except Exception as e:
            self.fail("Page doesn't load: " + str(e))
        select = Select(self.driver.find_element(By.ID, "State-dropdown"))
        select.select_by_visible_text("TX")
        apply = self.driver.find_element(By.ID, "apply")
        self.driver.execute_script("arguments[0].click();", apply)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-35"))
            )
        except Exception as e:
            self.fail("State filter doesn't work: " + str(e))

    def testSearch(self):
        """
        Make sure searching works.
        """
        # Wait for the park cards to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-1"))
            )
        except Exception as e:
            self.fail("Page doesn't load: " + str(e))
        self.driver.find_element(By.ID, "search").send_keys("big bend")
        self.driver.find_element(By.CLASS_NAME, "search-button").click()
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-35"))
            )
        except Exception as e:
            self.fail("Searching parks doesn't work: " + str(e))

    def testSort(self):
        """
        Make sure sorting works.
        """
        # Wait for the park cards to load first
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-1"))
            )
        except Exception as e:
            self.fail("Page doesn't load: " + str(e))
        select = Select(self.driver.find_element(By.ID, "Sort-dropdown"))
        select.select_by_visible_text("Name (Z-A)")
        apply = self.driver.find_element(By.ID, "apply")
        self.driver.execute_script("arguments[0].click();", apply)
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "park-card-466"))
            )
        except Exception as e:
            self.fail("Sorting parks doesn't work: " + str(e))

