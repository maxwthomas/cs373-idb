from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import unittest

URL = "https://dev.parkdex.me"

class HomeTest(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service, options=options)
        self.driver.get(URL)
  
    def tearDown(self) -> None:
        self.driver.quit()

    def testParksLink(self):
        """
        Make sure park images leads to correct page
        """
        elem = self.driver.find_element(By.ID, "model-card-parks")
        self.driver.execute_script("arguments[0].click();", elem)
        self.assertEqual(self.driver.current_url, URL + "/parks")

    def testHotelsLink(self):
        """
        Make sure hotel images leads to correct page
        """
        elem = self.driver.find_element(By.ID, "model-card-hotels")
        self.driver.execute_script("arguments[0].click();", elem)
        self.assertEqual(self.driver.current_url, URL + "/hotels")

    def testAirportsLink(self):
        """
        Make sure airport images leads to correct page
        """
        elem = self.driver.find_element(By.ID, "model-card-airports")
        self.driver.execute_script("arguments[0].click();", elem)
        self.assertEqual(self.driver.current_url, URL + "/airports")
