from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from webdriver_manager.chrome import ChromeDriverManager
import unittest
from time import sleep

URL = "https://dev.parkdex.me/about"

class AboutTest(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        service = Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service, options=options)
        self.driver.get(URL)
  
    def tearDown(self) -> None:
        self.driver.quit()

    def testGitStats(self):
        """
        Make sure git stats are loaded.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                lambda _: self.driver.find_element(By.CSS_SELECTOR, "#git-stats > div").text != "0"
            )
        except Exception as e:
            self.fail("Git stats not loaded: " + str(e))

    def testTeamCards(self):
        """
        Make sure team cards load.
        """
        try:
            WebDriverWait(self.driver, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "card-eminalparslan"))
            )
        except Exception as e:
            self.fail("Card not found: " + str(e))

    def testToolsAccordion(self):
        """
        Click on frontend section under tools and make sure the React text can be seen.
        """
        acc = self.driver.find_element(By.CSS_SELECTOR, "#acc-item-Frontend button")
        # acc.click() doesn't work for some reason. have to use this workaround
        self.driver.execute_script("arguments[0].click();", acc)
        elem = self.driver.find_element(By.ID, "tool-React")
        sleep(0.3)
        self.assertEqual(elem.text, "React")
