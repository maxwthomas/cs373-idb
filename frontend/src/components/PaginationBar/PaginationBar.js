import React from "react";
import "./PaginationBar.css";

const PaginationBar = ({ onPageChange, totalPages, currentPage, pageSize, className }) => {
  const onNext = () => {
    if (currentPage == totalPages) return;
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    if (currentPage <= 1) return;
    onPageChange(currentPage - 1);
  };
  // let lastPage = paginationRange[paginationRange.length - 1];
  return (
    <div className="pagination-bar">
      <div
        className={`pagination-arrow ${currentPage <= 1 ? "disabled" : ""}`}
        onClick={() => onPrevious()}
      >
        <i className="fa-solid fa-chevron-left"></i>
      </div>
      <p>
        {currentPage} of {totalPages}
      </p>
      <div
        id="next-button"
        className={`pagination-arrow ${currentPage == totalPages ? "disabled" : ""}`}
        onClick={() => onNext()}
      >
        <i className="fa-solid fa-chevron-right"></i>
      </div>
    </div>
  );
};

export default PaginationBar;
