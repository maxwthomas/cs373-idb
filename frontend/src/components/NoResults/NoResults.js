import React from "react";
import "./NoResults.css";

export const NoResults = () => {
  return (
    <div className="noresults-container">
      <i class="fa-solid fa-magnifying-glass"></i>
      <p>{"No Results Found :("}</p>
    </div>
  );
};
