import React from "react";
import { Link } from "react-router-dom";
import "./ModelTable.css";
import { useNavigate, useLocation } from "react-router-dom";
import Highlighter from "react-highlight-words";
import { getByTestId } from "@testing-library/react";

const ModelTable = ({ data, columns, modelName, highlights }) => {
  const navigate = useNavigate();
  const location = useLocation();
  console.log("highlights", highlights);
  return (
    <table className="model-table">
      <thead>
        <tr>
          {columns.map((item, index) => (
            <TableHeadItem item={item} key={index} />
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((row, index) => (
          <TableRow
            row={row}
            columns={columns}
            key={index}
            id={row.id}
            dataTestId={`${modelName}-card`}
            navigate={navigate}
            location={location}
            highlights={highlights}
          />
        ))}
      </tbody>
    </table>
  );
};

const TableHeadItem = ({ item }) => <th>{item.header}</th>;

const TableRow = ({ row, columns, id, dataTestId, navigate, location, highlights }) => (
  <tr
    id={`${dataTestId}-${row.id}`}
    data-testid={dataTestId}
    onClick={() => navigate(`${location.pathname}/${id}`)}
  >
    {columns.map((columnItem, index) => {
      return (
        <td key={index}>
          <Highlighter
            searchWords={highlights}
            autoEscape={true}
            textToHighlight={row[columnItem.value] ? row[columnItem.value].toString() : ""}
          />
        </td>
      );
    })}
  </tr>
);

export default ModelTable;
