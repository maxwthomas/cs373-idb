import { Route, Routes } from "react-router-dom";
import { useEffect } from "react";

import React from "react";
import { useLocation } from "react-router-dom";
import Home from "../pages/Home/Home";
import About from "../pages/About/About";
import ProviderVisualizations from "../pages/Visualizations/ProviderVisualizations";
import Visualizations from "../pages/Visualizations/Visualizations";
import Hotels from "../pages/Hotels/Hotels";
import Parks from "../pages/Parks/Parks";
import Airports from "../pages/Airports/Airports";
import ParkInstance from "../pages/ParkInstance/ParkInstance";
import HotelInstance from "../pages/HotelInstance/HotelInstance";
import AirportInstance from "../pages/AirportInstance/AirportInstance";
import { ThemeProvider } from "react-bootstrap";
import Search from "../pages/Search/Search";

const Section = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <ThemeProvider breakpoints={["xxxl", "xxl", "xl", "lg", "md", "sm", "xs"]} minBreakpoint="xs">
      <section style={pathname != "/" && pathname != "/about" ? { marginTop: "100px" } : {}}>
        <Routes>
          <Route path="/" element={<Home />} exact />
          <Route exact path="/about" element={<About />} />
          <Route exact path="/provider-visualizations" element={<ProviderVisualizations />} />
          <Route exact path="/visualizations" element={<Visualizations />} />
          <Route path="/parks" element={<Parks />} />
          <Route path="/parks/:id" element={<ParkInstance />} />
          <Route path="/hotels" element={<Hotels />} />
          <Route path="/hotels/:id" element={<HotelInstance />} />
          <Route path="/airports" element={<Airports />} />
          <Route path="/airports/:id" element={<AirportInstance />} />
          <Route path="/search" element={<Search />} />
        </Routes>
      </section>
    </ThemeProvider>
  );
};

export default Section;
