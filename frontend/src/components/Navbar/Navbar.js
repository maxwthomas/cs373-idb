import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import Tree from "../../assets/tree.png";

const Navbar = () => {
  const [active, setActive] = useState("Home");

  const [scrollPosition, setScrollPosition] = useState(0);

  const handleScroll = () => {
    const position = window.scrollY;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const makeHidden = (page) => {
    if (page === "Home" || page === "About") {
      if (scrollPosition < window.innerHeight) {
        return "navbar-hidden";
      }
    } else {
      if (scrollPosition < 100) {
        return "navbar-hidden";
      }
    }
    return "";
  };

  return (
    <nav className={"navbar fixed-top navbar-expand-lg navbar-dark p-md-3"}>
      <div className="container">
        <Link to="/" className="navbar-brand">
          <img src={Tree} style={{ height: "35px" }} alt="logo" />
          <span>Parkdex</span>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className={"nav-item"}>
              <Link to="/" onClick={() => setActive("Home")} className="nav-link">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/parks" onClick={() => setActive("Parks")} className="nav-link">
                Parks
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/hotels" onClick={() => setActive("Hotels")} className="nav-link">
                Hotels
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/airports" onClick={() => setActive("Airports")} className="nav-link">
                Airports
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/visualizations"
                onClick={() => setActive("Visualizations")}
                className="nav-link"
              >
                Visualizations
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/provider-visualizations"
                onClick={() => setActive("ProviderVisualizations")}
                className="nav-link"
              >
                Provider Visualizations
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" onClick={() => setActive("About")} className="nav-link">
                About
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/search" onClick={() => setActive("Search")} className="nav-link">
                <i className="fas fa-search"></i>
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className={`navbar-background ${makeHidden(active)}`}></div>
    </nav>
  );
};

export default Navbar;
