import React, { useEffect, useState } from "react";
import "./Select.css";

const Select = ({ label, options, onChange }) => {
  const [value, setValue] = useState(null);

  function handleChange(e) {
    setValue(e.target.value);
  }

  useEffect(() => {
    onChange(value);
  }, [value]);

  return (
    <div className="select-container">
      <label htmlFor="">{label}</label>
      <div className="select-dropdown">
        <select id={`${label}-dropdown`} onChange={handleChange}>
          <option value=""></option>
          {options.map((option, index) => (
            <option value={option} key={index}>
              {option}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default Select;
