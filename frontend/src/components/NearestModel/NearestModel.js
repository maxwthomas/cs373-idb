import React from "react";
import { Link } from "react-router-dom";
import "./NearestModel.css";
const NearestModel = ({ icon, title, list, color, type }) => {
  return (
    <div className="nearestmodel-container">
      <div className="header">
        <i className={icon}></i>
        {title}
      </div>
      <div className="items">
        {list.map((item, index) => (
          <Link
            className="item"
            to={`/${type}/${item.id}`}
            style={{ backgroundColor: color }}
            key={index}
          >
            {item.name}
          </Link>
        ))}
      </div>
    </div>
  );
};

export default NearestModel;
