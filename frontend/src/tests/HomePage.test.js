import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Home from "../pages/Home/Home";

describe("Home page testing", () => {
  test("Page renders without crashing", async () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const elem = screen.getByText("Plan for a trip");
    expect(elem).toBeInTheDocument();
  });

  test("Model page cards render", async () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const elem = screen.getByTestId("model-card-parks");
    expect(elem).toBeInTheDocument();
  });
});
