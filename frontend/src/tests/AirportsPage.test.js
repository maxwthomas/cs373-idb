import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Airports from "../pages/Airports/Airports";

describe("Parks page testing", () => {
  test("Page renders correctly", async () => {
    render(
      <MemoryRouter>
        <Airports />
      </MemoryRouter>
    );
    const elems = screen.getAllByText(/airports/i);
    expect(elems.length).toBeGreaterThan(0);
  });

  test("Airport cards render", async () => {
    render(
      <MemoryRouter>
        <Airports />
      </MemoryRouter>
    );
    const elems = await screen.findAllByTestId("airport-card");
    expect(elems.length).toBeGreaterThan(0);
  });
});
