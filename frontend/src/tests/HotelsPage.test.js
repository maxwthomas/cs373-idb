import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Hotels from "../pages/Hotels/Hotels";

describe("Parks page testing", () => {
  test("Page renders correctly", async () => {
    render(
      <MemoryRouter>
        <Hotels />
      </MemoryRouter>
    );
    const elems = screen.getAllByText(/hotels/i);
    expect(elems.length).toBeGreaterThan(0);
  });
});
