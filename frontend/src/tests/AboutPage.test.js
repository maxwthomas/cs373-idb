import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import About from "../pages/About/About";

describe("About page testing", () => {
  test("Page renders without crashing", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByText("About");
    expect(elem).toBeInTheDocument();
  });

  test("Icon tooltips render", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByText("Our GitLab");
    expect(elem).toBeInTheDocument();
  });

  test("Total git stats render", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByTestId("git-stats");
    expect(elem).toBeInTheDocument();
  });

  test("Team section renders", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByTestId("team");
    expect(elem).toBeInTheDocument();
  });

  test("Tools section renders", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByText("Tools");
    expect(elem).toBeInTheDocument();
  });

  test("Tool cards render", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByText("Discord");
    expect(elem).toBeInTheDocument();
  });

  test("Scrape section renders", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = screen.getByText("How was the data scraped?");
    expect(elem).toBeInTheDocument();
  });

  test("Team cards render", async () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const elem = await screen.findByText("Emin Arslan", {}, { timeout: 5000 });
    expect(elem).toBeInTheDocument();
  });
});
