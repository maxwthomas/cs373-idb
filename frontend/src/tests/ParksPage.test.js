import { MemoryRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import Parks from "../pages/Parks/Parks";

describe("Parks page testing", () => {
  test("Page renders correctly", async () => {
    render(
      <MemoryRouter>
        <Parks />
      </MemoryRouter>
    );
    const elems = screen.getAllByText(/parks/i);
    expect(elems.length).toBeGreaterThan(0);
  });
});
