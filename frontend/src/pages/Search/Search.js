import React from "react";
import "./Search.css";
import Parks from "../Parks/Parks";
import Hotels from "../Hotels/Hotels";
import Airports from "../Airports/Airports";
import { useState } from "react";

function Home() {
  const [search, setSearch] = useState("");

  function handleSearch(e) {
    e.preventDefault();
    setSearch(e.target.search.value);
  }
  return (
    <div className="search-page-container">
      <form onSubmit={handleSearch} className="searchbar-container">
        <input id="search" type="text" className="searchbar" />
        <button type="submit" className="search-button">
          Search
        </button>
      </form>
      {search ? content(search) : <></>}
    </div>
  );
}

function content(search) {
  return (
    <>
      <Parks searchInput={search} />
      <Airports searchInput={search} />
      <Hotels searchInput={search} />
    </>
  );
}
export default Home;
