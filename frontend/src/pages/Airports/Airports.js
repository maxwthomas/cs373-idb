import React, { useState, useEffect } from "react";
import axios from "axios";
import { Airport, AirportList } from "./AirportList.js";
import { Link } from "react-router-dom";
import ModelTable from "../../components/ModelTable/ModelTable.js";
import PaginationBar from "../../components/PaginationBar/PaginationBar.js";
import { Spinner } from "react-bootstrap";
import Select from "../../components/Select/Select.js";
import FilterOptions from "../../data/filterLists";
import { NoResults } from "../../components/NoResults/NoResults.js";

const Airports = ({ searchInput }) => {
  const [airports, setAirports] = useState(null);
  const [loading, setLoading] = useState(true);
  const [currPage, setCurrPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalItems, setTotalItems] = useState("-");
  const [highlights, setHighlights] = useState([]);
  const [search, setSearch] = useState(searchInput);
  const [state, setState] = useState(null);
  const [type, setType] = useState(null);
  const [has_website, setHasWebsite] = useState(null);
  const [has_scheduling_service, setHasSchedulingService] = useState(null);
  const [has_wiki_page, setHasWikiPage] = useState(null);
  const [sort, setSort] = useState(null);
  const [desc, setDesc] = useState(null);

  const attributes = {
    state,
    type,
    has_website,
    has_scheduling_service,
    has_wiki_page,
    sort,
    desc,
  };

  function onSortChange(option) {
    if (!option) return;
    let split = option.split(" ");
    setSort(split[0].toLowerCase());
    setDesc(split[1] == "(Z-A)");
  }

  function callAPI() {
    setLoading(true);
    let filters = "";
    for (let attribute in attributes) {
      if (attributes[attribute]) filters += `&${attribute}=${encodeURI(attributes[attribute])}`;
    }
    let searchURI = search ? `&q=${encodeURI(search)}` : "";
    let url = `https://api.parkdex.me/airports?page=${currPage}${filters}${searchURI}`;
    console.log(url);
    axios.get(url).then((res) => {
      // creates a AirportList of Airport objects
      // this allows us to extract logic into those classes
      setAirports(new AirportList(...res.data.data.map((e) => new Airport(e))));
      setTotalPages(res.data.total_pages);
      setTotalItems(res.data.count);
      setHighlights(res.data.search_terms);
    });
  }
  useEffect(() => {
    callAPI();
  }, [currPage]);

  useEffect(() => {
    if (airports) setLoading(false);
  }, [airports]);

  useEffect(() => {
    callAPI();
  }, [currPage]);

  useEffect(() => {
    setSearch(searchInput);
  }, [searchInput]);

  function onChange(setOption, option) {
    setOption(option);
  }

  function onChangeBool(setOption, option) {
    if (!option) setOption(null);
    setOption(option == "Yes" ? true : false);
  }

  function onApply() {
    callAPI();
  }

  function handleSearch(e) {
    e.preventDefault();
    console.log(e.target.search.value);
    setSearch(e.target.search.value);
  }

  useEffect(() => {
    if (!search) return;
    callAPI();
  }, [search]);

  const columns = [
    {
      header: "Name",
      value: "name",
    },
    {
      header: "Type",
      value: "type",
    },
    ,
    {
      header: "Schedule Service",
      value: "has_schedule_service",
    },
    {
      header: "Elevation",
      value: "elevation",
    },
    {
      header: "Municipality",
      value: "municipality",
    },
    {
      header: "State",
      value: "state",
    },
    {
      header: "Zip",
      value: "zip",
    },
  ];

  return (
    <div className="section-container">
      <div className="models-title">
        <h1>
          Airports <p>{totalItems}</p>
        </h1>
      </div>
      <form onSubmit={handleSearch} className="searchbar-container">
        <input id="search" type="text" className="searchbar" />
        <button type="submit" className="search-button">
          Search
        </button>
      </form>
      <div className="filters-container">
        <div className="filter-options">
          <Select
            label="State"
            options={FilterOptions.states}
            onChange={(option) => onChange(setState, option)}
          />
          <Select
            label="Type"
            options={["small_airport", "medium_airport", "large_airport"]}
            onChange={(option) => onChange(setType, option)}
          />

          <Select
            label="Website"
            options={["Yes", "No"]}
            onChange={(option) => onChangeBool(setHasWebsite, option)}
          />
          <Select
            label="SS"
            options={["Yes", "No"]}
            onChange={(option) => onChangeBool(setHasSchedulingService, option)}
          />
          <Select
            label="Wiki Page"
            options={["Yes", "No"]}
            onChange={(option) => onChangeBool(setHasWikiPage, option)}
          />
          <Select
            label="Sort"
            options={["Name (A-Z)", "Name (Z-A)", "State (A-Z)", "State (Z-A)"]}
            onChange={onSortChange}
          />
        </div>
        <button id="apply" onClick={onApply}>
          Apply
        </button>
      </div>
      {!loading ? (
        <div className="models-page-content" style={{ flexDirection: "column" }}>
          {totalPages == 0 ? (
            <NoResults />
          ) : (
            <ModelTable
              data={airports}
              columns={columns}
              modelName="airport"
              highlights={highlights}
            />
          )}

          <PaginationBar
            currentPage={totalPages == 0 ? 0 : currPage}
            totalPages={totalPages}
            onPageChange={(page) => setCurrPage(page)}
          />
        </div>
      ) : (
        <div style={{ margin: "50px" }}>
          <Spinner animation="border" />
        </div>
      )}
    </div>
  );
};

export default Airports;
