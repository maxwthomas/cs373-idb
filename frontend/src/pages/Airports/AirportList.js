export class Airport {
  constructor(airportObj) {
    Object.assign(this, airportObj);
  }
}

export class AirportList extends Array {
  getParkById(id) {
    return this.find(e => e.id === id);
  }
}
