import React, { useState, useEffect } from "react";
import axios from "axios";
import { Hotel, HotelList } from "./HotelList.js";
import { Link } from "react-router-dom";
import ModelTable from "../../components/ModelTable/ModelTable.js";
import PaginationBar from "../../components/PaginationBar/PaginationBar.js";
import { Spinner } from "react-bootstrap";
import Select from "../../components/Select/Select.js";
import FilterOptions from "../../data/filterLists";
import { NoResults } from "../../components/NoResults/NoResults.js";

const Hotels = ({ searchInput }) => {
  const [hotels, setHotels] = useState(null);
  const [loading, setLoading] = useState(true);
  const [currPage, setCurrPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalItems, setTotalItems] = useState("-");
  const [highlights, setHighlights] = useState([]);
  const [search, setSearch] = useState(searchInput);
  const [state, setState] = useState(null);
  const [county, setCounty] = useState(null);
  const [brand, setBrand] = useState(null);
  const [has_website, setHasWebsite] = useState(null);
  const [has_wiki_page, setHasWikiPage] = useState(null);
  const [sort, setSort] = useState(null);
  const [desc, setDesc] = useState(null);
  const attributes = { state, county, brand, has_website, has_wiki_page, sort, desc };

  function callAPI() {
    setLoading(true);
    let filters = "";
    for (let attribute in attributes) {
      if (attributes[attribute]) filters += `&${attribute}=${encodeURI(attributes[attribute])}`;
    }
    let searchURI = search ? `&q=${encodeURI(search)}` : "";
    let url = `https://api.parkdex.me/hotels?page=${currPage}${filters}${searchURI}`;
    console.log(url);
    axios.get(url).then((res) => {
      // creates a ParkList of Park objects
      // this allows us to extract logic into those classes
      setHotels(new HotelList(...res.data.data.map((e) => new Hotel(e))));
      setTotalPages(res.data.total_pages);
      setTotalItems(res.data.count);
      setHighlights(res.data.search_terms);
    });
  }

  useEffect(() => {
    callAPI();
  }, [currPage]);

  useEffect(() => {
    setSearch(searchInput);
  }, [searchInput]);

  useEffect(() => {
    if (hotels) setLoading(false);
  }, [hotels]);

  function onChange(setOption, option) {
    setOption(option);
  }

  function onApply() {
    callAPI();
  }

  function onSortChange(option) {
    if (!option) return;
    let split = option.split(" ");
    setSort(split[0].toLowerCase());
    setDesc(split[1] == "(Z-A)");
  }

  function onChangeBool(setOption, option) {
    if (!option) setOption(null);
    setOption(option == "Yes" ? true : false);
  }

  function handleSearch(e) {
    e.preventDefault();
    console.log(e.target.search.value);
    setSearch(e.target.search.value);
  }

  useEffect(() => {
    if (!search) return;
    callAPI();
  }, [search]);

  const columns = [
    {
      header: "Name",
      value: "name",
    },
    {
      header: "Brand",
      value: "brand",
    },
    {
      header: "City",
      value: "city",
    },
    {
      header: "State",
      value: "state",
    },
    {
      header: "Zip",
      value: "zip",
    },
    {
      header: "Website",
      value: "website",
    },
  ];
  return (
    <div className="section-container">
      <div className="models-title">
        <h1>
          Hotels <p>{totalItems}</p>
        </h1>
      </div>
      <form onSubmit={handleSearch} className="searchbar-container">
        <input id="search" type="text" className="searchbar" />
        <button type="submit" className="search-button">
          Search
        </button>
      </form>
      <div className="filters-container">
        <div className="filter-options">
          <Select
            label="State"
            options={FilterOptions.states}
            onChange={(option) => onChange(setState, option)}
          />
          <Select
            label="Counties"
            options={FilterOptions.counties}
            onChange={(option) => onChange(setCounty, option)}
          />
          <Select
            label="Brands"
            options={FilterOptions.brands}
            onChange={(option) => onChange(setBrand, option)}
          />
          <Select
            label="Website"
            options={["Yes", "No"]}
            onChange={(option) => onChangeBool(setHasWebsite, option)}
          />
          <Select
            label="Wiki Page"
            options={["Yes", "No"]}
            onChange={(option) => onChangeBool(setHasWikiPage, option)}
          />
          <Select
            label="Sort"
            options={["Name (A-Z)", "Name (Z-A)", "State (A-Z)", "State (Z-A)"]}
            onChange={onSortChange}
          />
        </div>
        <button id="apply" onClick={onApply}>
          Apply
        </button>
      </div>
      {!loading ? (
        <div className="models-page-content" style={{ flexDirection: "column" }}>
          {totalPages == 0 ? (
            <NoResults />
          ) : (
            <ModelTable data={hotels} columns={columns} modelName="hotel" highlights={highlights} />
          )}
          <PaginationBar
            currentPage={totalPages == 0 ? 0 : currPage}
            totalPages={totalPages}
            onPageChange={(page) => setCurrPage(page)}
          />
        </div>
      ) : (
        <div style={{ margin: "50px" }}>
          <Spinner animation="border" />
        </div>
      )}
    </div>
  );
};

export default Hotels;
