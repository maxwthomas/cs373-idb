export class Hotel {
  constructor(hotelObj) {
    Object.assign(this, hotelObj);
  }
}

export class HotelList extends Array {
  getParkById(id) {
    return this.find(e => e.id === id);
  }
}
