import React, { useState, useEffect } from "react";
import axios from "axios";
import { Park, ParkList } from "./ParkList";
import "./Parks.css";
import { Link } from "react-router-dom";
import PaginationBar from "../../components/PaginationBar/PaginationBar";
import { Spinner } from "react-bootstrap";
import Select from "../../components/Select/Select";
import FilterOptions from "../../data/filterLists";
import Highlighter from "react-highlight-words";
import { NoResults } from "../../components/NoResults/NoResults";

const Parks = ({ searchInput }) => {
  const [parks, setParks] = useState(null);
  const [loading, setLoading] = useState(true);
  const [currPage, setCurrPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [totalItems, setTotalItems] = useState("-");
  const [highlights, setHighlights] = useState([]);
  const [search, setSearch] = useState(searchInput);
  const [state, setState] = useState(null);
  const [designation, setDesignation] = useState(null);
  const [topic, setTopic] = useState(null);
  const [activity, setActivity] = useState(null);
  const [sort, setSort] = useState(null);
  const [desc, setDesc] = useState(null);

  const attributes = { state, designation, topic, activity, sort, desc };

  useEffect(() => {
    setSearch(searchInput);
  }, [searchInput]);

  useEffect(() => {
    // api call (to local development backend for now)
    callAPI();
  }, [currPage]);

  function callAPI() {
    setLoading(true);
    let filters = "";
    for (let attribute in attributes) {
      if (attributes[attribute]) filters += `&${attribute}=${encodeURI(attributes[attribute])}`;
    }
    let searchURI = search ? `&q=${encodeURI(search)}` : "";
    let url = `https://api.parkdex.me/parks?page=${currPage}${filters}${searchURI}`;
    console.log(url);
    axios.get(url).then((res) => {
      console.log(res);
      setParks(new ParkList(...res.data.data.map((e) => new Park(e))));
      setTotalPages(res.data.total_pages);
      setTotalItems(res.data.count);
      setHighlights(res.data.search_terms);
    });
    setSearch("");
  }

  useEffect(() => {
    if (parks) setLoading(false);
  }, [parks]);

  const onPageChange = (page) => {
    setCurrPage(page);
  };

  const weekdays = [
    {
      label: "M",
      name: "monday",
    },
    {
      label: "T",
      name: "tuesday",
    },
    {
      label: "W",
      name: "wednesday",
    },
    {
      label: "Th",
      name: "thursday",
    },
    {
      label: "F",
      name: "friday",
    },
    {
      label: "S",
      name: "saturday",
    },
    {
      label: "Su",
      name: "sunday",
    },
  ];

  function onSortChange(option) {
    if (!option) return;
    let split = option.split(" ");
    setSort(split[0].toLowerCase());
    setDesc(split[1] == "(Z-A)");
  }

  function onChange(setOption, option) {
    setOption(option);
  }

  function onApply() {
    callAPI();
  }

  function handleSearch(e) {
    e.preventDefault();
    setSearch(e.target.search.value);
  }

  useEffect(() => {
    if (!search) return;
    callAPI();
  }, [search]);

  return (
    <div className="section-container">
      <div className="models-title">
        <h1>
          Parks <p>{totalItems}</p>
        </h1>
      </div>
      <form onSubmit={handleSearch} className="searchbar-container">
        <input id="search" type="text" className="searchbar" />
        <button type="submit" className="search-button">
          Search
        </button>
      </form>
      <div className="filters-container">
        <div className="filter-options">
          <Select
            label="State"
            options={FilterOptions.states}
            onChange={(option) => onChange(setState, option)}
          />
          <Select
            label="Designation"
            options={FilterOptions.designations}
            onChange={(option) => onChange(setDesignation, option)}
          />
          <Select
            label="Topic"
            options={FilterOptions.topics}
            onChange={(option) => onChange(setTopic, option)}
          />
          <Select
            label="Activity"
            options={FilterOptions.topics}
            onChange={(option) => onChange(setActivity, option)}
          />
          <Select
            label="Sort"
            options={["Name (A-Z)", "Name (Z-A)", "State (A-Z)", "State (Z-A)"]}
            onChange={onSortChange}
          />
        </div>
        <button id="apply" onClick={onApply}>
          Apply
        </button>
      </div>

      {!loading ? (
        <div className="models-page-content">
          <div className="total-items"></div>
          {totalPages == 0 ? (
            <NoResults />
          ) : (
            <div className="models-container">
              {parks.map((park, index) => (
                <Link
                  id={`park-card-${park.id}`}
                  data-testid={"park-card"}
                  key={park.id}
                  to={`/parks/${park.id}`}
                  className="park-card"
                >
                  <div className="image-container">
                    <img alt="" src={park.images[0] ? park.images[0].url : ""} />
                    <p>
                      <i className="fa-solid fa-location-dot"></i>
                      <Highlighter
                        // highlightClassName="YourHighlightClass"
                        searchWords={highlights}
                        autoEscape={true}
                        textToHighlight={`${park.city},${park.state}`}
                      />
                    </p>
                  </div>
                  <div className="content">
                    <h1>
                      <Highlighter
                        searchWords={highlights}
                        autoEscape={true}
                        textToHighlight={park.name}
                      />
                    </h1>
                    <p>
                      <Highlighter
                        // highlightClassName="YourHighlightClass"
                        searchWords={highlights}
                        autoEscape={true}
                        textToHighlight={park.designation}
                      />
                    </p>
                    <div className="weekdays">
                      {weekdays.map((day, index) => (
                        <div
                          className={`weekday-circle ${
                            park.hours[day.name] == null ? "closed" : ""
                          }`}
                          key={index}
                        >
                          <p>{day.label}</p>
                        </div>
                      ))}
                    </div>

                    <div className="activities tags">
                      {park.activities.slice(0, 4).map((park, index) => (
                        <div className="activity tag-item" key={index}>
                          <p>{park.length > 10 ? park.substring(0, 7) + ".." : park}</p>
                        </div>
                      ))}
                    </div>
                    <div className="topics tags">
                      {park.topics.slice(0, 4).map((park, index) => (
                        <div className="topic tag-item" key={index}>
                          <p>{park.length > 10 ? park.substring(0, 7) + ".." : park}</p>
                        </div>
                      ))}
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          )}
          <PaginationBar
            currentPage={totalPages == 0 ? 0 : currPage}
            totalPages={totalPages}
            onPageChange={onPageChange}
          />
        </div>
      ) : (
        <div style={{ margin: "50px" }}>
          <Spinner animation="border" />
        </div>
      )}
    </div>
  );
};

export default Parks;
