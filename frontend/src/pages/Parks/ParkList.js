export class Park {
  constructor(parkObj) {
    Object.assign(this, parkObj);
    this.activities = this.activities.filter((e) => e !== "");
    this.topics = this.topics.filter((e) => e !== "");
  }
}

export class ParkList extends Array {
  getParkById(id) {
    return this.find(e => e.id === id);
  }
}
