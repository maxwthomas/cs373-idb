import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Airport } from "../Airports/AirportList";
import Spinner from "react-bootstrap/Spinner";
import NearestModel from "../../components/NearestModel/NearestModel";
import "./AirportInstance.css";
import { MAP_API_KEY } from "../../MapAPIKey";

const AirportInstance = () => {
  const [details, setDetails] = useState(null);
  const airportId = useParams().id;
  const [size, setSize] = useState(null);

  useEffect(() => {
    axios.get(`https://api.parkdex.me/airports/${airportId}`).then((res) => {
      setDetails(new Airport(res.data));
      const size = res.data.type.split("_")[0];
      setSize(size);
    });
  }, [airportId]);

  useEffect(() => {
    console.log(details);
  }, [details]);

  return (
    <div className="section-container">
      <div className="instance-content">
        {details === null ? (
          <Spinner animation="border" />
        ) : (
          <div>
            <div className="airport-header">
              <h1>{details.name}</h1>
              <div className="icao">
                <h3>{details.icao}</h3>
              </div>
            </div>
            <h2 style={{ color: "#5353533" }}>
              {details.municipality}, {details.state}
            </h2>

            <div className="airport-attribute">
              <h3>
                Size: <span>{size}</span>
              </h3>
            </div>
            <div className="airport-attribute">
              <h3>
                Elevation: <span>{details.elevation}</span>
              </h3>
            </div>
            <iframe
              title="map"
              className="map"
              loading="lazy"
              allowFullScreen
              referrerPolicy="no-referrer-when-downgrade"
              src={`https://www.google.com/maps/embed/v1/place?key=${MAP_API_KEY}&q=${details.latitude}+${details.longitude}`}
            ></iframe>
            <div className="near-models">
              <NearestModel
                title="Nearest Parks"
                list={details.nearest_national_parks}
                color="#C2E5CA"
                icon="fa-solid fa-tree"
                type="parks"
              />
              <NearestModel
                title="Nearest Hotels"
                list={details.nearest_hotels}
                color="#C2DFE5"
                type="hotels"
                icon="fa-solid fa-hotel"
              />
            </div>
            {/* <ul>
              <li>
                <p>
                  website: <a href={details.website}>{details.website}</a>
                </p>
              </li>
              <li>
                <p>elevation: {details.elevation}</p>
              </li>
              <li>
                <p>Map display of location</p>
              </li>
              <ul>
                <li>
                  <p>longitude: {details.long}</p>
                </li>
                <li>
                  <p>lattitude: {details.lat}</p>
                </li>
              </ul>
              <li>
                <p>Nearest Hotels:</p>
                <ul>
                  {details.nearest_hotels.map((hotel, index) => (
                    <li key={index}>
                      <Link to={`/hotels/${hotel.id}`}>{hotel.name}</Link>
                    </li>
                  ))}
                </ul>
              </li>
              <li>
                <p>Nearest National Parks:</p>
                <ul>
                  {details.nearest_national_parks.map((park, index) => (
                    <li key={index}>
                      <Link to={`/parks/${park.id}`}>{park.name}</Link>
                    </li>
                  ))}
                </ul>
              </li>
            </ul> */}
          </div>
        )}
      </div>
    </div>
  );
};

export default AirportInstance;
