import { React, useEffect, useState } from "react";
import { Container, Row, Col, Image, Card, Alert, Spinner, Tooltip, OverlayTrigger, Accordion } from "react-bootstrap";
import axios from "axios";

import { teamInfo } from "./TeamInfo";
import { iconsInfo } from "./IconInfo";
import { toolsInfo } from "./ToolInfo";

import gitIcon from "../../assets/git.svg";
import issueIcon from "../../assets/issue.svg";
import testIcon from "../../assets/test-tube.svg";

import "./About.css";

const About = () => {
  const [team, setTeam] = useState([]);
  const [numCommits, setNumCommits] = useState(0);
  const [numIssues, setNumIssues] = useState(0);
  const [numTests, setNumTests] = useState(0);

  useEffect(() => {
    (async () => {
      let res;
      let people = [];
      let totalCommits = 0;
      res = await axios.get("https://gitlab.com/api/v4/projects/39574602/repository/contributors");
      people = res.data;
      people.forEach((e) => {
        let person = teamInfo.find((p) => p.emails.includes(e.email));
        person.commits = e.commits;
        totalCommits += e.commits;
      });
      setNumCommits(totalCommits);

      const perPage = 100;
      let curIssues = [];
      let allIssues = [];
      do {
        res = await axios.get(
          `https://gitlab.com/api/v4/projects/39574602/issues?scope=all&per_page=${perPage}`
        );
        curIssues = res.data;
        allIssues.push(...curIssues);
      } while (curIssues.length === perPage);
      setNumIssues(allIssues.length);

      let totalTests = 0;
      teamInfo.forEach((p) => {
        p.issues = 0
        totalTests += p.tests;
      });
      setNumTests(totalTests);

      allIssues.forEach((e) => {
        let assignee = e.assignee;
        if (assignee) {
          let person = teamInfo.find((p) => p.usernames.includes(assignee.username));
          person.issues++;
        }
      });
      setTeam(teamInfo);
    })();
  }, []);

  const gitInfo = (name, icon, num, placement) => (
    <OverlayTrigger
      placement={placement}
      overlay={<Tooltip>{name}</Tooltip>}
    >
      {({ ref, ...triggerHandler }) => (
        <Col {...triggerHandler}>
          <Image ref={ref} className="mx-2" style={{ height: 28 }} src={icon} alt={name} />
          {num}
        </Col>
      )}
    </OverlayTrigger>
  )

  return (
    <Container fluid className="m-0 p-0 d-flex flex-column align-items-center">
      <Row className="about-banner-image w-100 vh-100 d-flex justify-content-center align-items-center m-0">
        <h1 className="about-splash-title">“What a country chooses to save is what a country chooses to say about itself.” - Mollie Beattie</h1>
      </Row>
      <Col md={8} xxxl={6}>
        <h1 className="section-title">About</h1>
        <Row className="px-4">
          <p className="description">
            A webpage that displays all of the national parks, airports, and hotels in the
            United States with the goal of letting a user find a national park they would like
            to visit and then find nearby transportation and lodging to make that visit
            possible. The national parks are a part of the US government services and are great
            display of natural beauty and this site would encorage users to go visit!
          </p>
          <p className="description">
            By looking for nearby hotels and nearby airports to a National Park, we learned that 
            most National Parks, even if they are relatively remote, will almost certainly have 
            hotels and airports close by. This tells us that most National Parks are popular 
            tourist destinations because of the nearby lodging and transportation.
          </p>
        </Row>
        <Row className="justify-content-center">
          {iconsInfo.map((icon) => (
            <OverlayTrigger
              key={icon.name}
              placement="bottom"
              show="true"
              overlay={<Tooltip className="mt-1" style={{ zIndex: -2 }}>{icon.name}</Tooltip>}
            >
              {({ ref, ...triggerHandler }) => (
                <Col {...triggerHandler} xs={3} className="d-flex justify-content-center">
                  <a 
                    target="_blank"
                    rel="noreferrer"
                    href={icon.link}
                  >
                    <img
                      ref={ref}
                      className="mx-4"
                      style={{ height: 100 }}
                      src={icon.icon}
                      alt={`${icon.name} icon`}
                    />
                  </a>
                </Col>
              )}
            </OverlayTrigger>
          ))}
        </Row>
        <h1 data-testid="team" className="section-title">Team</h1>
        <Col xs={6} className="mx-auto">
          <Alert id="git-stats" data-testid="git-stats" variant="secondary" className="d-flex align-items-center text-center mb-5">
            {gitInfo("Total Commits", gitIcon, numCommits, "bottom")}
            {gitInfo("Total Issues", issueIcon, numIssues, "bottom")}
            {gitInfo("Total Tests", testIcon, numTests, "bottom")}
          </Alert>
        </Col>
      </Col>
      <Col md={12} xxxl={8}>
        {team.length === 0 ? <Spinner animation="border" /> : 
          <Row xs={1} md={2} lg={3} className="g-4 mx-5 justify-content-center">
            {team.map((person) => (
              <Col className="card-col" key={person.name}>
                <Card
                  id={`card-${person.usernames[0]}`}
                  data-testid={`card-${person.usernames[0]}`}
                  className="h-100">
                  <a target="_blank" rel="noreferrer" href={`https://gitlab.com/${person.usernames[0]}`}>
                    <Card.Img variant="top" src={person.image} />
                  </a>
                  <Card.Body>
                    <Row className="align-items-center">
                      <Card.Title className="col">{person.name}</Card.Title>
                      <Card.Title className="col text-muted text-end">{person.role}</Card.Title>
                    </Row>
                    <Card.Text>{person.bio}</Card.Text>
                  </Card.Body>
                  <Card.Footer className="d-flex align-items-center text-center">
                    {gitInfo("Commits", gitIcon, person.commits, "top")}
                    {gitInfo("Issues", issueIcon, person.issues, "top")}
                    {gitInfo("Tests", testIcon, person.tests, "top")}
                  </Card.Footer>
                </Card>
              </Col>
            ))}
          </Row>
        }
      </Col>
      <h1 className="section-title">Tools</h1>
      <Col md={8} xxxl={6}>
        <Accordion defaultActiveKey="0">
          {toolsInfo.map((info, index) => (
            <Accordion.Item key={info.title} eventKey={index}>
              <Accordion.Header id={`acc-item-${info.title}`}>{info.title}</Accordion.Header>
              <Accordion.Body>
                <Row md={3} className="m-2 g-2 justify-content-center">
                  {info.toolInfo.map((tool) => (
                    <Col key={tool.name}>
                      <Card className="h-100">
                        <Card.Img
                          style={{ margin: "30px auto 15px auto", width: "75%" }}
                          variant="top" src={tool.icon}
                        />
                        <Card.Body className="d-flex flex-column justify-content-end">
                          <Card.Title id={`tool-${tool.name}`}>{tool.name}</Card.Title>
                          <Card.Text>{tool.description}</Card.Text>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))}
                </Row>
              </Accordion.Body>
            </Accordion.Item>
          ))}
        </Accordion>
        <h1 className="section-title">How was the data scraped?</h1>
        <Row className="px-4">
          <p className="description">
            Each API was programmatically scraped using the Python3 
            requests library. An API key was acquired for each source and plugged into the 
            HTTP GET request for each API call. We were then able to use the documentation 
            from each source to determine the proper query parameters to use for each API 
            call. The results of each API call were then dumped into a JSON file.
          </p>
        </Row>
      </Col>
    </Container>
  );
};

export default About;
