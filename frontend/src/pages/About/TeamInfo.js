import MaxImage from "../../assets/Max.jpg"
import EminImage from "../../assets/Emin.jpg"
import JustinImage from "../../assets/Justin.jpg"
import LawrenceImage from "../../assets/Lawrence.jpg";
import ManishImage from "../../assets/Manish.jpg"

export const teamInfo = [
  {
    name: "Emin Arslan",
    usernames: ["eminalparslan"],
    emails: ["eminalparslan@gmail.com"],
    role: "Frontend API",
    bio: "I'm a sophomore majoring in CS at UT Austin. I like playing the piano, bouldering, and spending time with friends.",
    commits: 0,
    issues: 0,
    tests: 32,
    image: EminImage,
  },
  {
    name: "Justin Xie",
    usernames: ["celebi1023", "Lawrence"],
    emails: ["justin23@cs.utexas.edu", "justinxie23@gmail.com"],
    role: "Backend",
    bio: "I'm a senior studying computer science. I'm from Houston, TX, and I enjoy reading in my free time.",
    commits: 0,
    issues: 0,
    tests: 9,
    image: JustinImage,
  },
  {
    name: "Lawrence Zhang",
    usernames: ["lawrencezhang1"],
    emails: ["lzlawrencezhang@gmail.com"],
    role: "Cloud",
    bio: "I'm a native Austinite and sophomore studying Computer Science at UT Austin. I'm interested in Competitive Programming and Machine Learning, and love to boulder and play video games whenever I have time. Feel free to reach out to me at lawrencezhang@utexas.edu!",
    commits: 0,
    issues: 0,
    tests: 16,
    image: LawrenceImage,
  },
  {
    name: "Manish Bhandari",
    usernames: ["manishbh"],
    emails: ["bhmanish73@gmail.com"],
    role: "Frontend UI",
    bio: "I'm a senior studying Computer Science and Digital Arts & Media at UT Austin. I'm a full stack developer with a keen interest in design. I enjoy dancing with my collegiate dance team and on my free time I like to take on photography and videography gigs around campus.",
    commits: 0,
    issues: 0,
    tests: 0,
    image: ManishImage,
  },
  {
    name: "Max Thomas",
    usernames: ["maxwthomas"],
    emails: ["max.thomas@outlook.com"],
    role: "DevOps",
    bio: "I'm a senior CS major and Business minor student from Dallas, TX. I also work as a software engineer and in my free time I like to boulder, cook, and workout!",
    commits: 0,
    issues: 0,
    tests: 0,
    image: MaxImage
  },
]