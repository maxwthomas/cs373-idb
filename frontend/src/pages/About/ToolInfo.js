import gitlabIcon from "../../assets/gitlab.svg";
import postmanIcon from "../../assets/postman.svg";
import discordIcon from "../../assets/discord.svg";
import namecheapIcon from "../../assets/namecheap.svg";
import reactIcon from "../../assets/react.svg";
import bootstrapIcon from "../../assets/bootstrap.svg";
import AWSIcon from "../../assets/amazon-aws.svg";
import flaskIcon from "../../assets/flask.svg";
import flaskMarshmallowIcon from "../../assets/flask-marshmallow.png";
import flaskSQLAlchemyIcon from "../../assets/sqlalchemy.png";
import nationalParkServiceIcon from "../../assets/national-park-service.png";
import rapidapiIcon from "../../assets/rapidapi.png";
import geoapifyIcon from "../../assets/geoapify.jpg";

const teamToolsInfo = [
  {
    name: "Discord",
    description: "Team communication platform",
    icon: discordIcon,
    link: "https://discord.com/",
  },
  {
    name: "GitLab",
    description: "Git repository hosting and CI/CD platform",
    icon: gitlabIcon,
    link: "https://gitlab.com/maxwthomas/cs373-idb/",
  },
  {
    name: "Namecheap",
    description: "To obtain a free domain name",
    icon: namecheapIcon,
    link: "https://www.namecheap.com/",
  }
]

const frontendToolsInfo = [
  {
    name: "React",
    description: "JavaScript library for front-end development",
    icon: reactIcon,
    link: "https://reactjs.org/",
  },
  {
    name: "Bootstrap",
    description: "CSS framework for front-end development",
    icon: bootstrapIcon,
    link: "https://getbootstrap.com/",
  },
  {
    name: "AWS Amplify",
    description: "Cloud hosting platform for the web application",
    icon: AWSIcon,
    link: "https://aws.amazon.com/amplify/",
  }
]

const backendToolsInfo = [
  {
    name: "Flask",
    description: "Micro web framework for the API development",
    icon: flaskIcon,
    link: "https://flask.palletsprojects.com/en/2.2.x/",
  },
  {
    name: "Flask SQLAlchemy",
    description: "Provides SQLAlchemy functionality to our Flask app",
    icon: flaskSQLAlchemyIcon,
    link: "https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/",
  },
  {
    name: "Flask Marshmallow",
    description: "Allows us to define schemas to connect data to models",
    icon: flaskMarshmallowIcon,
    link: "https://flask-marshmallow.readthedocs.io/en/latest/",
  },
  {
    name: "Postman",
    description: "To test and document our API",
    icon: postmanIcon,
    link: "https://documenter.getpostman.com/view/23609863/2s83menNo2",
  }
]

const dataToolsInfo = [
  {
    name: "National Park Services",
    description: "",
    icon: nationalParkServiceIcon,
    link: "https://www.nps.gov/subjects/developer/api-documentation.htm#/parks/getPark",
  },
  {
    name: "RapidAPI",
    description: "",
    icon: rapidapiIcon,
    link: "https://rapidapi.com/sujayvsarma/api/ourairport-data-search",
  },
  {
    name: "Geoapify",
    description: "",
    icon: geoapifyIcon,
    link: "https://apidocs.geoapify.com/docs/places/#about",
  }
]

export const toolsInfo = [
  {
    title: "Team",
    toolInfo: teamToolsInfo,
  },
  {
    title: "Frontend",
    toolInfo: frontendToolsInfo,
  },
  {
    title: "Backend",
    toolInfo: backendToolsInfo,
  },
  {
    title: "Data",
    toolInfo: dataToolsInfo,
  }
]
