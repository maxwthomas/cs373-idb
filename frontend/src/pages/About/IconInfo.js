import gitlabIcon from "../../assets/gitlab.svg";
import postmanIcon from "../../assets/postman.svg";

export const iconsInfo = [
  {
    name: "Our GitLab",
    icon: gitlabIcon,
    link: "https://gitlab.com/maxwthomas/cs373-idb/",
  },
  {
    name: "Postman Docs",
    icon: postmanIcon,
    link: "https://documenter.getpostman.com/view/23609863/2s83menNo2",
  }
]