import React from "react";
import "./Home.css";
import ParkSplash from "../../assets/park-splash.jpg";
import HotelSplash from "../../assets/hotel-splash.jpg";
import AirportSplash from "../../assets/airport-splash.jpg";

import { Link } from "react-router-dom";

const quotes = [
  <h1 className="splash-title">
    “Between every two pine trees there is a door leading to a new way of life.”
    <br />
    <i>John Muir</i>
  </h1>,
  <h1 className="splash-title">
    “In wilderness is the preservation of the world.”
    <br />
    <i>Henry David Thoreau</i>
  </h1>,
  <h1 className="splash-title">
    “Within National Parks is room – glorious room – room in which to find ourselves, in which to
    think and hope, to dream and plan, to rest and resolve.”
    <br />
    <i>Enos Mills</i>
  </h1>,
];

function Home() {
  return (
    <div>
      <div className="banner-image w-100 vh-100 d-flex justify-content-center align-items-center">
        {quotes[Math.floor(Math.random() * 3)]}
      </div>
      <div className="models-section">
        <div className="models-header">
          <h1>Plan for a trip</h1>
          <p>Explore your options and find the trip suitable for you.</p>
        </div>
        <div className="home-models-container">
          <ModelCard image={ParkSplash} title={"Parks"} />
          <ModelCard image={HotelSplash} title={"Hotels"} />
          <ModelCard image={AirportSplash} title={"Airports"} />
        </div>
      </div>
    </div>
  );
}

const ModelCard = ({ image, title }) => {
  return (
    <Link
      id={`model-card-${title.toLowerCase()}`}
      data-testid={`model-card-${title.toLowerCase()}`}
      to={`/${title.toLowerCase()}`} className="model-card"
    >
      <img className="model-splash-image" src={image} alt="" />
      <h1>{title}</h1>
    </Link>
  );
};

export default Home;
