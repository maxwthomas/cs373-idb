import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Park } from "../Parks/ParkList";
import Spinner from "react-bootstrap/Spinner";
import "./ParkInstance.css";
import NearestModel from "../../components/NearestModel/NearestModel";
import { MAP_API_KEY } from "../../MapAPIKey";

const ParkInstance = () => {
  const [details, setDetails] = useState(null);
  const parkId = useParams().id;

  useEffect(() => {
    axios.get(`https://api.parkdex.me/parks/${parkId}`).then((res) => {
      setDetails(new Park(res.data));
    });
  }, [parkId]);

  useEffect(() => {
    console.log(details);
  }, [details]);

  const weekdays = [
    {
      label: "Mon",
      key: "monday",
    },
    {
      label: "Tue",
      key: "tuesday",
    },
    {
      label: "Wed",
      key: "wednesday",
    },
    {
      label: "Thu",
      key: "thursday",
    },
    {
      label: "Fri",
      key: "friday",
    },
    {
      label: "Sat",
      key: "saturday",
    },
    {
      label: "Sun",
      key: "sunday",
    },
  ];

  return (
    <div className="section-container">
      {details === null ? (
        <Spinner animation="border" />
      ) : (
        <div className="instance-content">
          <h1>{details.name}</h1>
          {/* <p>
              {details.addr} {details.city} {details.zip}
            </p> */}
          <p>{details.description}</p>

          <div className="images-gallery">
            {details.images.map((image, index) => (
              <div key={index} className="image">
                <img src={image.url} alt="" />
              </div>
            ))}
          </div>

          <div className="tags-and-hours">
            <div className="instance-tags">
              <div className="tag-container">
                <h4>Activities</h4>
                <div className="tag-content">
                  {details.activities.map((activity, index) => (
                    <div className="tag-outline" key={index}>
                      {activity}
                    </div>
                  ))}
                </div>
              </div>
              <div className="tag-container">
                <h4>Topics</h4>
                <div className="tag-content">
                  {details.topics.map((topic, index) => (
                    <div className="tag-outline" key={index}>
                      {topic}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="hours">
              <h4>Hours</h4>
              <div className="hours-content">
                {weekdays.map((day, index) => {
                  return (
                    <div
                      className={`hour-row ${details.hours[day.key] ? "" : "closed"}`}
                      key={index}
                    >
                      <p className="weekday-label">{day.label}</p>
                      <p className={`hour`}>
                        {details.hours[day.key] ? details.hours[day.key] : "closed"}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <iframe
          title="map"
          className="map"
          loading="lazy"
          allowFullScreen
          referrerPolicy="no-referrer-when-downgrade"
          src={`https://www.google.com/maps/embed/v1/place?key=${MAP_API_KEY}&q=${details.latitude}+${details.longitude}`}>
          </iframe>
          <div className="near-models">
            <NearestModel
              title="Nearest Hotels"
              list={details.nearest_hotels}
              color="#C2E5CA"
              icon="fa-solid fa-hotel"
              type="hotels"
            />
            <NearestModel
              title="Nearest Airports"
              list={details.nearest_airports}
              color="#C2DFE5"
              type="airports"
              icon="fa-solid fa-plane"
            />
          </div>

          {/* <ul>
              <li>
                <p>
                  website: <a href={details.website}>{details.website}</a>
                </p>
              </li>
              <li>
                <p>Map display of location</p>
              </li>
              <ul>
                <li>
                  <p>longitude: {details.long}</p>
                </li>
                <li>
                  <p>lattitude: {details.lat}</p>
                </li>
              </ul>
              <li>
                <p>Nearest Hotels:</p>
                <ul>
                  {details.nearest_hotels.map((hotel, index) => (
                    <li key={index}>
                      <Link to={`/hotels/${hotel.id}`}>{hotel.name}</Link>
                    </li>
                  ))}
                </ul>
              </li>
              <li>
                <p>Nearest Airports:</p>
                <ul>
                  {details.nearest_airports.map((airport, index) => (
                    <li key={index}>
                      <Link to={`/airports/${airport.id}`}>{airport.name}</Link>
                    </li>
                  ))}
                </ul>
              </li>
            </ul> */}
        </div>
      )}
    </div>
  );
};

export default ParkInstance;
