import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Hotel } from "../Hotels/HotelList";
import Spinner from "react-bootstrap/Spinner";
import NearestModel from "../../components/NearestModel/NearestModel";
import "./HotelInstance.css";
import { MAP_API_KEY } from "../../MapAPIKey";

const HotelInstance = () => {
  const [details, setDetails] = useState(null);
  const hotelId = useParams().id;

  useEffect(() => {
    axios.get(`https://api.parkdex.me/hotels/${hotelId}`).then((res) => {
      setDetails(new Hotel(res.data));
    });
  }, [hotelId]);

  useEffect(() => {
    console.log(details);
  }, [details]);

  // TODO: temporary images, need to scrape pics from google
  const hotelImages = [
    "https://media-cdn.tripadvisor.com/media/photo-s/25/04/93/1e/blossom-hotel-houston.jpg",
    "https://eastaustinhotel.com/wp-content/uploads/2019/06/home.jpg",
    "https://cdn2.hubspot.net/hubfs/439788/Blog/Featured%20Images/Best%20Hotel%20Website%20Designs.jpg",
    "https://assets.tivolihotels.com/image/upload/q_auto,f_auto,c_limit,w_1378/media/minor/tivoli/images/brand_level/footer/1920x1000/thr_aboutus1_1920x1000.jpg",
  ];
  return (
    <div className="section-container">
      {details === null ? (
        <Spinner animation="border" />
      ) : (
        <div className="instance-content">
          <h1>
            {details.name}{" "}
            <a href={details.website}>
              <i className="fa-solid fa-link"></i>
            </a>
          </h1>
          <p>
            {details.addr} {details.city} {details.state} {details.zip}
          </p>
          <p>Brand: {details.brand}</p>
          <div className="gallery-and-map">
            <div className="images-gallery">
              {hotelImages.map((image, index) => (
                <div className="image" key={index}>
                  <img src={image}></img>
                </div>
              ))}
            </div>
            <iframe
              title="map"
              className="map"
              loading="lazy"
              allowFullScreen
              referrerPolicy="no-referrer-when-downgrade"
              src={`https://www.google.com/maps/embed/v1/place?key=${MAP_API_KEY}&q=${details.latitude}+${details.longitude}`}
            ></iframe>
          </div>
          <div className="near-models">
            <NearestModel
              title="Nearest Parks"
              list={details.nearest_national_parks}
              color="#C2E5CA"
              icon="fa-solid fa-tree"
              type="parks"
            />
            <NearestModel
              title="Nearest Airports"
              list={details.nearest_airports}
              color="#C2DFE5"
              type="airports"
              icon="fa-solid fa-plane"
            />
          </div>
          {/* <ul>
            <li>
              <p>
                website: <a href={details.website}>{details.website}</a>
              </p>
            </li>
            <li>
              <p>
                wiki: <a href={details.website_wiki}>{details.website_wiki}</a>
              </p>
            </li>
            <li>
              <p>Map display of location</p>
            </li>
            <ul>
              <li>
                <p>longitude: {details.long}</p>
              </li>
              <li>
                <p>lattitude: {details.lat}</p>
              </li>
            </ul>
            <li>
              <p>Nearest Parks:</p>
              <ul>
                {details.nearest_national_parks.map((park, index) => (
                  <li key={index}>
                    <Link to={`/parks/${park.id}`}>{park.name}</Link>
                  </li>
                ))}
              </ul>
            </li>
            <li>
              <p>Nearest Airports:</p>
              <ul>
                {details.nearest_airports.map((airport, index) => (
                  <li key={index}>
                    <Link to={`/airports/${airport.id}`}>{airport.name}</Link>
                  </li>
                ))}
              </ul>
            </li>
          </ul> */}
        </div>
      )}
    </div>
  );
};

export default HotelInstance;
