import React, { useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import axios from "axios";
import { Chart } from "chart.js/auto";

const ProviderVisualizations = () => {

  useEffect(() => {
    cityRatingChart();
    eventTypeChart();
    restaurantRatingChart();
  }, []);

  const cityRatingChart = async () => {
    const url = "https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/cities";
    const cities = await axios.get(`${url}?sort=population&desc`);
    const city_chart = cities.data.data.map((city) => ({x: city.city_population, y: city.city_avg_rating}));
    new Chart(
      document.getElementById("city_rating_chart"),
      {
        type: "scatter",
        data: {
          datasets: [{
            data: city_chart,
            backgroundColor: [
              'rgb(93, 95, 113)'
            ]
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false
            }
          },
          scales: {
            x: {
              title: {
                display: true,
                text: 'Population'
              },
              beginAtZero: false
            },
            y: {
              title: {
                display: true,
                text: 'Rating'
              },
              beginAtZero: false
            }
          }
        }
      }
    );
  }

  const eventTypeChart = async () => {
    const url = "https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/events";
    const event_types = [
      "Arts & Theatre",
      "Film",
      "Music",
      "Sports"
    ]
    const results = await Promise.all(event_types.map((event_type) => axios.get(`${url}?type=${event_type}`)));
    const type_counts = results.map((res) => res.data.data.length);
    new Chart(
      document.getElementById("event_type_chart"),
      {
        type: "pie",
        data: {
          labels: event_types,
          datasets: [{
            label: "Total Number of Events",
            data: type_counts,
            backgroundColor: [
              'rgb(5, 47, 95)',
              'rgb(0, 83, 119)',
              'rgb(6, 167, 125)',
              'rgb(213, 198, 122)'
            ],
            hoverOffset: 4,
          }]
        },
      }
    );
  }

  const restaurantRatingChart = async () => {
    const url = "https://outnabout.p3eas2rb02ene.us-east-2.cs.amazonlightsail.com/api/restaurants";
    const restaurants = await axios.get(`${url}`);
    const states = new Set(restaurants.data.data.map((restaurant) => restaurant.rest_state));
    const results = await Promise.all([...states].map((state) => axios.get(`${url}?state=${state}`)));
    const state_ratings = results.map((res) => ({
      state: res.data.data[0].rest_state,
      avg_rating: res.data.data.map((entry) => entry.rest_rating).reduce((a, b) => a + b) / res.data.data.length
    })).sort((a, b) => a.avg_rating < b.avg_rating ? 1 : -1);
    const top10_states = state_ratings.map((entry) => entry.state);
    const top10_ratings = state_ratings.map((entry) => entry.avg_rating);
    new Chart(
      document.getElementById("restaurant_rating_chart"),
      {
        type: "bar",
        data: {
          labels: top10_states,
          datasets: [{
            label: "Average Restaurant Rating",
            data: top10_ratings,
            backgroundColor: [
              'rgb(5, 74, 145)',
              'rgb(62, 124, 177)',
              'rgb(129, 164, 205)',
              'rgb(219, 228, 238)',
              'rgb(241, 115, 0)'
            ],
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false
            }
          },
          scales: {
            y: {
              beginAtZero: false
            }
          }
        },
      }
    );
  }

  return (
    <Container fluid className="m-0 p-0 d-flex flex-column align-items-center">
      <Col sm={6} md={4} className="mt-5 pt-5 d-flex flex-column align-items-center">
        <Row className="pb-5">
          <h1>Out N About Visualizations</h1>
        </Row>
        <Row className="p-0 my-3">
          <h3>City population to city average rating</h3>
          <canvas id="city_rating_chart"></canvas>
        </Row>
        <Row className="p-0 my-3">
          <h3>Most common event types held</h3>
          <canvas id="event_type_chart"></canvas>
        </Row>
        <Row className="p-0 my-3">
          <h3>States ordered by restaurant ratings</h3>
          <canvas id="restaurant_rating_chart"></canvas>
        </Row>
      </Col>
    </Container>
  );
}

export default ProviderVisualizations;
