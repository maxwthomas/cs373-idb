import React, { useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import axios from "axios";
import { Chart } from "chart.js/auto";

const Visualizations = () => {

  useEffect(() => {
    parkStateChart();
    hotelBrandChart();
    airportSizeChart();
  }, []);

  const parkStateChart = async () => {
    const url = "https://api.parkdex.me/parks";
    // These states were calculated with the commented out lines below
    // Querying for all 50 states each time the visualization was loaded was
    // slow and inefficient, so the top 10 states are hardcoded for now.
    const states = ["CA", "DC", "NY", "NM", "VA", "MD", "AZ", "AK", "PA", "MA"];
    //const states = filterLists.states;
    const results = await Promise.all(states.map((state) => axios.get(`${url}?state=${state}`)));
    //const state_counts = new Map(states.map((state, i) => [state, results[i].data.count]));
    //const sorted_state_counts = new Map([...state_counts].sort((a, b) => a[1] < b[1]));
    //const top10_states = [...sorted_state_counts.keys()].slice(0, 10);
    //const top10_counts = [...sorted_state_counts.values()].slice(0, 10);
    const top10_states = states;
    const top10_counts = results.map((res) => res.data.count);
    new Chart(
      document.getElementById("park_state_chart"),
      {
        type: "bar",
        data: {
          labels: top10_states,
          datasets: [{
            label: "Parks",
            data: top10_counts,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(255, 159, 64, 0.2)',
              'rgba(255, 205, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(201, 203, 207, 0.2)'
            ],
            borderColor: [
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
              'rgb(75, 192, 192)',
              'rgb(54, 162, 235)',
              'rgb(153, 102, 255)',
              'rgb(201, 203, 207)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false
            }
          },
          scales: {
            y: {
              beginAtZero: true
            }
          }
        },
      }
    );
  }

  const hotelBrandChart = async () => {
    const url = "https://api.parkdex.me/hotels";
    const brands = ["Ramada", "Hilton", "Marriott", "Comfort Inn", "Best Western"];
    const results = await Promise.all(brands.map((brand) => axios.get(`${url}?brand=${encodeURI(brand)}`)));
    const brand_counts = results.map((res) => res.data.count);
    new Chart(
      document.getElementById("hotel_brand_chart"),
      {
        type: "polarArea",
        data: {
          labels: brands,
          datasets: [{
            label: "Hotels",
            data: brand_counts,
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(75, 192, 192)',
              'rgb(255, 205, 86)',
              'rgb(201, 203, 207)',
              'rgb(54, 162, 235)'
            ]
          }]
        }
      }
    );
  }

  const airportSizeChart = async () => {
    const url = "https://api.parkdex.me/airports";
    const sizes = [
      "small",
      "medium",
      "large",
    ]
    const results = await Promise.all(sizes.map((size) => axios.get(`${url}?type=${size}_airport`)));
    const [small_airports, medium_airports, large_airports] = results.map((res) => res.data.count);
    new Chart(
      document.getElementById("airport_size_chart"),
      {
        type: "pie",
        data: {
          labels: sizes,
          datasets: [{
            label: "Airports",
            data: [small_airports, medium_airports, large_airports],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            hoverOffset: 4,
          }]
        },
      }
    );
  }

  return (
    <Container fluid className="m-0 p-0 d-flex flex-column align-items-center">
      <Col sm={6} md={4} className="mt-5 pt-5 d-flex flex-column align-items-center">
        <Row className="pb-5">
          <h1>Our Visualizations</h1>
        </Row>
        <Row className="p-0 my-3">
          <h3>States with the most national parks</h3>
          <canvas id="park_state_chart"></canvas>
        </Row>
        <Row className="p-0 my-3">
          <h3>Number of some popular hotel chains</h3>
          <canvas id="hotel_brand_chart"></canvas>
        </Row>
        <Row className="p-0 my-3">
          <h3>Number of small, medium, and large airports</h3>
          <canvas id="airport_size_chart"></canvas>
        </Row>
      </Col>
    </Container>
  );
}

export default Visualizations;
