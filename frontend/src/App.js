import Navbar from "./components/Navbar/Navbar";
import { BrowserRouter as Router } from "react-router-dom";
import Section from "./components/Section";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Section />
      </div>
    </Router>
  );
}

export default App;
