# Backend

## Make targets
1. `make docker-install` - creates the docker image which installs all the necessary dependencies to run the backend server in the 'backend' docker image
2. `make docker-run` - runs the 'backend' docker image
3. `make install` - installs all the necessary dependencies to run the backend server
4. `make run` - runs the backend server at http://localhost:5000
5. `make test` - runs all available backend tests
6. `make test-unit` - runs backend unit tests
7. `make test-acceptance` - runs backend acceptance tests
8. `make format` - formats the backend code
9. `make format-check` - verifys that the backend code is correctly formatted
10. `make build` - does nothing
11. `make clean` - removes all artificats needed to run the backend server
NOTE: `make docker-install` and `make install` only needs to be run with NEW requirements.txt, otherwise `make docker-run` and `make run` can be run once and changes should cause an auto-reload.
