from models import app, Parks, Airports, Hotels
from utils import query_model_multiple, query_model_single, format_park
from flask import request


@app.route("/parks")
def get_parks():
    return query_model_multiple(Parks, request.args, format_park)


@app.route("/airports")
def get_airports():
    return query_model_multiple(Airports, request.args)


@app.route("/hotels")
def get_hotels():
    return query_model_multiple(Hotels, request.args)


@app.route("/parks/<id>")
def get_park(id):
    return query_model_single(Parks, id, format_park)


@app.route("/airports/<id>")
def get_airport(id):
    return query_model_single(Airports, id)


@app.route("/hotels/<id>")
def get_hotel(id):
    return query_model_single(Hotels, id)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
