from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
CORS(app)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://admin:Parkdex373@parkdex-cloud-db.cif1r9mabi9m.us-east-2.rds.amazonaws.com:3306/parkdex"
app.config["CORS_HEADERS"] = "Content-Type"
db = SQLAlchemy(app)


class Parks(db.Model):
    __tablename__ = "national_parks"
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    designation = db.Column(db.String)
    description = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    weather = db.Column(db.String)
    activities = db.Column(db.String)
    topics = db.Column(db.String)
    state = db.Column(db.String)
    city = db.Column(db.String)
    zip = db.Column(db.String)
    addr = db.Column(db.String)
    monday = db.Column(db.String)
    tuesday = db.Column(db.String)
    wednesday = db.Column(db.String)
    thursday = db.Column(db.String)
    friday = db.Column(db.String)
    saturday = db.Column(db.String)
    sunday = db.Column(db.String)
    price = db.Column(db.Integer)
    images = db.Column(db.JSON)
    nearest_airports = db.Column(db.JSON)
    nearest_hotels = db.Column(db.JSON)
    filters = {"state", "designation"}
    list_filters = {"activity": "activities", "topic": "topics"}
    has_filters = set()
    max_filters = {"price"}
    search_weights = {
        "name": 1000,
        "designation": 50,
        "description": 20,
        "latitude": 5,
        "longitude": 5,
        "weather": 30,
        "activities": 100,
        "topics": 100,
        "state": 100,
        "city": 100,
        "zip": 10,
        "addr": 20,
        "monday": 5,
        "tuesday": 5,
        "wednesday": 5,
        "thursday": 5,
        "friday": 5,
        "saturday": 5,
        "sunday": 5,
        "price": 10,
    }
    items_per_page = 20


class Airports(db.Model):
    __tablename__ = "airports"
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    type = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    elevation = db.Column(db.String)
    municipality = db.Column(db.String)
    has_schedule_service = db.Column(db.Integer)
    icao = db.Column(db.String)
    iata = db.Column(db.String)
    website = db.Column(db.String)
    website_wiki = db.Column(db.String)
    state = db.Column(db.String)
    zip = db.Column(db.String)
    nearest_hotels = db.Column(db.JSON)
    nearest_national_parks = db.Column(db.JSON)
    filters = {"has_schedule_service", "state", "type"}
    list_filters = set()
    has_filters = {"website", "website_wiki"}
    max_filters = set()
    search_weights = {
        "name": 1000,
        "type": 100,
        "latitude": 5,
        "longitude": 5,
        "elevation": 5,
        "has_schedule_service": 2,
        "icao": 500,
        "iata": 500,
        "website": 300,
        "website_wiki": 200,
        "state": 200,
        "zip": 10,
    }
    items_per_page = 30


class Hotels(db.Model):
    __tablename__ = "hotels"
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    type = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    addr = db.Column(db.String)
    city = db.Column(db.String)
    county = db.Column(db.String)
    state = db.Column(db.String)
    zip = db.Column(db.String)
    brand = db.Column(db.String)
    website = db.Column(db.String)
    website_wiki = db.Column(db.String)
    timezone = db.Column(db.String)
    nearest_airports = db.Column(db.JSON)
    nearest_national_parks = db.Column(db.JSON)
    filters = {"state", "county", "brand"}
    list_filters = set()
    has_filters = {"website", "website_wiki"}
    max_filters = set()
    search_weights = {
        "name": 1000,
        "type": 100,
        "latitude": 5,
        "longitude": 5,
        "addr": 100,
        "city": 300,
        "county": 200,
        "state": 100,
        "zip": 10,
        "brand": 500,
        "website": 30,
        "website_wiki": 30,
        "timezone": 800,
    }
    items_per_page = 30
