from unittest import main, TestCase
from app import app, Parks, Airports, Hotels
from collections import OrderedDict
import json

### HELPER FUNCTIONS ###
def sort_checker(tester, req, attribute, desc):
    data = json.loads(req.text, object_pairs_hook=OrderedDict)["data"]
    if not desc:
        tester.assertTrue(
            all(
                data[i][attribute] <= data[i + 1][attribute]
                if data[i][attribute] is not None and data[i + 1][attribute] is not None
                else True
                for i in range(len(data) - 1)
            ),
            [data[i][attribute] for i in range(len(data))],
        )
    else:
        tester.assertTrue(
            all(
                data[i][attribute] >= data[i + 1][attribute]
                if data[i][attribute] is not None and data[i + 1][attribute] is not None
                else True
                for i in range(len(data) - 1)
            ),
            [data[i][attribute] for i in range(len(data))],
        )


def filter_checker(tester, req, attribute, expected):
    data = json.loads(req.text, object_pairs_hook=OrderedDict)["data"]
    tester.assertTrue(all(data[i][attribute] == expected for i in range(len(data))))


def search_checker(tester, req, attribute, expected):
    data = json.loads(req.text, object_pairs_hook=OrderedDict)["data"]
    tester.assertTrue(data[0][attribute] == expected)


class TestModels(TestCase):
    ### BASIC GET TESTS ###
    def test_parks(self):
        test_client = app.test_client()
        req = test_client.get("/parks")
        self.assertEqual(req.status_code, 200)

    def test_parks_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/parks?page=2")
        self.assertEqual(req.status_code, 200)

    def test_parks_id_1(self):
        test_client = app.test_client()
        req = test_client.get("/parks/1")
        self.assertEqual(req.status_code, 200)

    def test_airports(self):
        test_client = app.test_client()
        req = test_client.get("/airports")
        self.assertEqual(req.status_code, 200)

    def test_airports_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/airports?page=2")
        self.assertEqual(req.status_code, 200)

    def test_airports_id_1(self):
        test_client = app.test_client()
        req = test_client.get("/airports/1")
        self.assertEqual(req.status_code, 200)

    def test_hotels(self):
        test_client = app.test_client()
        req = test_client.get("/hotels")
        self.assertEqual(req.status_code, 200)

    def test_hotels_page_2(self):
        test_client = app.test_client()
        req = test_client.get("/hotels?page=2")
        self.assertEqual(req.status_code, 200)

    def test_hotels_id_1(self):
        test_client = app.test_client()
        req = test_client.get("/hotels/1")
        self.assertEqual(req.status_code, 200)

    ### SORTING TESTS ###
    def test_parks_sorting_asc(self):
        test_client = app.test_client()
        park_attributes = Parks.filters

        for attribute in park_attributes:
            req = test_client.get("/parks?sort=" + attribute)
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, False)

    def test_parks_sorting_desc(self):
        test_client = app.test_client()
        park_attributes = Parks.filters

        for attribute in park_attributes:
            req = test_client.get("/parks?sort=" + attribute + "&desc=true")
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, True)

    def test_airports_sorting_asc(self):
        test_client = app.test_client()
        airport_attributes = Airports.filters

        for attribute in airport_attributes:
            req = test_client.get("/airports?sort=" + attribute)
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, False)

    def test_airports_sorting_desc(self):
        test_client = app.test_client()
        park_attributes = Airports.filters

        for attribute in park_attributes:
            req = test_client.get("/airports?sort=" + attribute + "&desc=true")
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, True)

    def test_hotels_sorting_asc(self):
        test_client = app.test_client()
        hotel_attributes = Hotels.filters

        for attribute in hotel_attributes:
            req = test_client.get("/hotels?sort=" + attribute)
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, False)

    def test_hotels_sorting_desc(self):
        test_client = app.test_client()
        hotel_attributes = Hotels.filters

        for attribute in hotel_attributes:
            req = test_client.get("/hotels?sort=" + attribute + "&desc=true")
            self.assertEqual(req.status_code, 200)
            sort_checker(self, req, attribute, True)

    ### FILTERING TESTS ###
    def test_parks_filter_state(self):
        test_client = app.test_client()
        req = test_client.get("/parks?state=KY")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "state", "KY")

    def test_parks_filter_state(self):
        test_client = app.test_client()
        req = test_client.get("/parks?designation=National+Park")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "designation", "National Park")

    def test_airports_filter_has_schedule_service(self):
        test_client = app.test_client()
        req = test_client.get("/airports?has_schedule_service=0")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "has_schedule_service", False)

    def test_airports_filter_state(self):
        test_client = app.test_client()
        req = test_client.get("/airports?state=TX")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "state", "TX")

    def test_airports_filter_type(self):
        test_client = app.test_client()
        req = test_client.get("/airports?type=small_airport")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "type", "small_airport")

    def test_hotels_filter_state(self):
        test_client = app.test_client()
        req = test_client.get("/hotels?state=NY")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "state", "NY")

    def test_hotels_filter_county(self):
        test_client = app.test_client()
        req = test_client.get("/hotels?county=Essex+County")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "county", "Essex County")

    def test_hotels_filter_brand(self):
        test_client = app.test_client()
        req = test_client.get("/hotels?brand=Hilton")
        self.assertEqual(req.status_code, 200)
        filter_checker(self, req, "brand", "Hilton")

    ### SEARCH TESTS ###
    def test_park_search(self):
        test_client = app.test_client()
        req = test_client.get("/parks?q=Alagnak+Wild+River")
        self.assertEqual(req.status_code, 200)
        search_checker(self, req, "name", "Alagnak Wild River")

    def test_airport_search(self):
        test_client = app.test_client()
        req = test_client.get("/airports?q=45+Ranch+Airport")
        self.assertEqual(req.status_code, 200)
        search_checker(self, req, "name", "45 Ranch Airport")

    def test_hotel_search(self):
        test_client = app.test_client()
        req = test_client.get("/hotels?q=Candlewood+Suites")
        self.assertEqual(req.status_code, 200)
        search_checker(self, req, "name", "Candlewood Suites")


if __name__ == "__main__":
    main()
