FROM python:3

WORKDIR /home

RUN apt update -y
RUN apt install -y default-libmysqlclient-dev
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN apt install -y npm
RUN npm install -g newman

EXPOSE 5000

CMD bash
