import requests
import json
import functools
import csv
import copy

import geopy
geolocator = geopy.Nominatim(user_agent="check_1")
import geopy.distance
from tzwhere import tzwhere


def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default


NATIONAL_PARKS_KEY = "7bDQytYctPCqwMonCnjhss3brjGoNlaWbn4qWjgy"
NATIONAL_PARKS_URL = "https://developer.nps.gov/api/v1/parks"
def scrape_national_parks():
    params = {"api_key": NATIONAL_PARKS_KEY, "limit": 5000}
    resp = requests.get(NATIONAL_PARKS_URL, params=params, timeout=1)
    if resp.status_code != 200:
        raise Exception("National Parks API returned non-200 status code: %d" % resp.status_code)

    id = 0
    cleaned_nps = []
    for np in resp.json().get("data"):
        # id, name, designation, description, lat, long, states, weather
        cleaned_np = {"id": id, "name": np.get("fullName"), "designation": np.get("designation"), "description": np.get("description"), "lat": safe_cast(np.get("latitude"), float), "long": safe_cast(np.get("longitude"), float), "weather": np.get("weatherInfo"), "website": np.get("url")}
        if cleaned_np.get("lat") is None or cleaned_np.get("long") is None:
            print("Bad National Park")
            continue
        if "addresses" not in np or len(np.get("addresses")) == 0:
            continue
        id += 1

        # activities, topics
        cleaned_np["activities"] = functools.reduce(lambda x, y: x + "\"" + y.get("name") + "\",", np.get("activities"), "")[:-1]
        cleaned_np["topics"] = functools.reduce(lambda x, y: x + "\"" + y.get("name") + "\",", np.get("topics"), "")[:-1]

        # address (pick first physical location)
        for addr in np.get("addresses"):
            if addr.get("type") == "Physical":
                cleaned_np["state"] = addr.get("stateCode")
                cleaned_np["city"] = addr.get("city")
                zipcode = addr.get("postalCode")
                if "-" in zipcode:
                    zipcode, _ = zipcode.split("-", 1)
                zipcode = safe_cast(zipcode, int)
                cleaned_np["zip"] = zipcode
                cleaned_np["addr"] = addr.get("line1")
                if len(addr.get("line2")) > 0:
                    cleaned_np["addr"] += ", %s" % addr.get("line2")
                if len(addr.get("line3")) > 0:
                    cleaned_np["addr"] += ", %s" % addr.get("line3")
                break

        # images (make sure each one is valid)
        cleaned_np["images"] = []
        for img in np.get("images"):
            try:
                resp = requests.get(img.get("url"), timeout=1)
                if resp.status_code != 200:
                    continue
            except:
                continue
            cleaned_np["images"].append({"title": img.get("title"), "url": img.get("url")})

        # monday, tuesday, ...
        if len(np.get("operatingHours")) > 0:
            for day, hours in np.get("operatingHours")[0].get("standardHours").items():
                cleaned_np[day.lower()] = hours if hours != "Closed" else None
        dows = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
        for dow in dows:
            if dow not in cleaned_np:
                cleaned_np[dow] = None

        # price (pick the cheapest)
        prices = set(map(lambda d : int(float(d.get("cost"))) if d.get("cost").replace(".", "").isdecimal() else None, np.get("entranceFees")))
        prices.discard(None) # Only thing left should be numbers
        if len(prices) > 0:
            cleaned_np["price"] = functools.reduce(lambda p1, p2: p1 if p1 < p2 else p2, prices)
        else:
            cleaned_np["price"] = None

        cleaned_nps.append(cleaned_np)
    return cleaned_nps


icao_to_iata = {}
with open('iata.csv') as f:
    for row in csv.reader(f):
        icao_to_iata[row[1]] = row[0]
AIRPORTS_KEY = "f96d585300mshdd0b87c9412a6a9p1b22dfjsn76d07e445428"
AIRPORTS_URL = "https://ourairport-data-search.p.rapidapi.com/api/airports/US"
AIRPORTS_HOST = "ourairport-data-search.p.rapidapi.com"
def scrape_airports(existing_airports=None):
    if existing_airports is None:
        headers = {"X-RapidAPI-Key": AIRPORTS_KEY, "X-RapidAPI-Host": AIRPORTS_HOST}
        resp = requests.get(AIRPORTS_URL, headers=headers)
        if resp.status_code != 200:
            raise Exception("Airports API returned non-200 status code: %d" % resp.status_code)

    id = 0
    cleaned_airports = []
    airport_list = existing_airports if existing_airports is not None else resp.json().get("results")
    for airport in airport_list:
        # API filtering params don't work great
        if existing_airports is None and (airport.get("country") != "US" or "airport" not in airport.get("type")):
            continue

        # id, name, type, lat, long, elevation, municipality, has_schedule_service, icao, iata, website, website_wiki
        cleaned_airport = {"id": airport.get("id", id), "name": airport.get("name"), "type": airport.get("type"), "lat": safe_cast(airport.get("lat"), float), "long": safe_cast(airport.get("lon"), float), "elevation": airport.get("elev"), "municipality": airport.get("municipality"), "has_schedule_service": airport.get("hasScheduledService"), "icao": airport.get("icao"), "website": airport.get("homepage"), "website_wiki": airport.get("wikipedia")}
        id += 1
        if id % 1000 == 0:
            print("Scraped %d airports" % id)

        # iata
        if airport.get("iata") is None and airport.get("icao") is not None:
            cleaned_airport['iata'] = icao_to_iata.get(airport["icao"])
        else:
            cleaned_airport['iata'] = airport.get("iata")

        # website
        if airport.get("website") is None and airport.get("icao") is not None:
            website = "https://airnav.com/airport/%s" % airport['icao']
            try:
                resp = requests.get(website, timeout=1)
                if resp.status_code == 200:
                    cleaned_airport['website'] = website
                else:
                    cleaned_airport['website'] = None
            except:
                cleaned_airport['website'] = None
        else:
            cleaned_airport['website'] = airport.get("website")

        # state
        state = airport.get("state")
        if state is None:
            _, state = airport.get("region").split('-')
        cleaned_airport['state'] = state

        # zip
        if "zip" not in airport:
            zipcode = geolocator.reverse("%f, %f" % (airport.get("lat"), airport.get("lon"))).raw['address'].get('postcode')
            if zipcode is not None:
                if "-" in zipcode:
                    zipcode, _ = zipcode.split("-", 1)
                zipcode = safe_cast(zipcode, int)
            cleaned_airport['zip'] = zipcode

        cleaned_airports.append(cleaned_airport)
    return cleaned_airports

def update_airports(airports):
    for airport in airports:
        if airport.get("website") is None and airport.get("icao") is not None:
            website = "https://airnav.com/airport/%s" % airport['icao']
            airport["website"] = website
        if airport.get("iata") is None and airport.get("icao") is not None:
            airport['iata'] = icao_to_iata.get(airport["icao"])
        else:
            airport['iata'] = airport.get("iata")
    return airports


HOTELS_KEY = "68a39b1beac74ed780acc18b3b89c609"
HOTELS_URL = "https://api.geoapify.com/v2/places"
HOTELS_DETAILS_URL = "https://api.geoapify.com/v2/places"
us_state_to_abbrev = {"Alabama": "AL","Alaska": "AK","Arizona": "AZ","Arkansas": "AR","California": "CA","Colorado": "CO","Connecticut": "CT","Delaware": "DE","Florida": "FL","Georgia": "GA","Hawaii": "HI","Idaho": "ID","Illinois": "IL","Indiana": "IN","Iowa": "IA","Kansas": "KS","Kentucky": "KY","Louisiana": "LA","Maine": "ME","Maryland": "MD","Massachusetts": "MA","Michigan": "MI","Minnesota": "MN","Mississippi": "MS","Missouri": "MO","Montana": "MT","Nebraska": "NE","Nevada": "NV","New Hampshire": "NH","New Jersey": "NJ","New Mexico": "NM","New York": "NY","North Carolina": "NC","North Dakota": "ND","Ohio": "OH","Oklahoma": "OK","Oregon": "OR","Pennsylvania": "PA","Rhode Island": "RI","South Carolina": "SC","South Dakota": "SD","Tennessee": "TN","Texas": "TX","Utah": "UT","Vermont": "VT","Virginia": "VA","Washington": "WA","West Virginia": "WV","Wisconsin": "WI","Wyoming": "WY","District of Columbia": "DC","American Samoa": "AS","Guam": "GU","Northern Mariana Islands": "MP","Puerto Rico": "PR","United States Minor Outlying Islands": "UM","U.S. Virgin Islands": "VI"}
def scrape_hotels(lat_long_points):
    tzw = tzwhere.tzwhere()
    id = 0
    cleaned_hotels = []
    for llp in lat_long_points:
        # Radius of 500000m should get everything we need...
        filter_string = "circle:%f,%f,500000" % (llp.get("long"), llp.get("lat"))
        bias_string = "proximity:%f,%f" % (llp.get("long"), llp.get("lat"))
        params = {"limit": 500, "apiKey": HOTELS_KEY, "categories": "accommodation.hotel", "filter": filter_string, "bias": bias_string}
        resp = requests.get(HOTELS_URL, params=params)
        if resp.status_code != 200:
            raise Exception("HOTELS API returned non-200 status code: %d" % resp.status_code)
        for hotel in resp.json().get("features"):
            # If hotel doesn't have name on record, its a bad record
            hotel = hotel.get("properties")
            if hotel.get("name") == None:
                continue
            # name, lat, long, addr, city, country, state, zip, brand, website
            cleaned_hotel = {"id": id, "name": hotel.get("name"), "lat": safe_cast(hotel.get("lat"), float), "long": safe_cast(hotel.get("lon"), float), "addr": hotel.get("housenumber", "") + " " + hotel.get("street", ""), "city": hotel.get("city"), "county": hotel.get("county"), "state": us_state_to_abbrev.get(hotel.get("state")), "zip": hotel.get("postcode"), "brand": hotel.get("datasource").get("raw").get("brand"), "website": hotel.get("datasource").get("raw").get("website")}
            id += 1
            if id % 1000 == 0:
                print("Scraped %d hotels" % id)
            # website_wiki
            if hotel.get("datasource").get("raw").get("brand:wikipedia") is not None:
                lang, page = hotel.get("datasource").get("raw").get("brand:wikipedia").split(":", 1)
                cleaned_hotel['website_wiki'] = "https://%s.wikipedia.org/wiki/%s" % (lang, page.replace(" ", "_"))
            else:
                cleaned_hotel['website_wiki'] = None
            # timezone
            cleaned_hotel['timezone'] = tzw.tzNameAt(hotel.get("lat"), hotel.get("lon"))
            cleaned_hotels.append(cleaned_hotel)
    return cleaned_hotels

# Look for unique hotels based on lat/long points
def cleanup_duplicate_hotels():
    with open("hotels.json", "r") as f:
        hotels = json.load(f)
    hotels = hotels.get("data")

    unique_hotels = list()
    hotel_points = set()
    for hotel in hotels:
        if hotel["id"] % 1000 == 0:
            print("Checked %d hotels" % hotel["id"])
        hotel_point = (hotel["lat"], hotel["long"])
        if hotel_point in hotel_points:
            continue
        else:
            hotel_points.add(hotel_point)
            unique_hotels.append(hotel)
    print("%d unique hotels" % len(unique_hotels))

    with open("unique-hotels.json", "w") as f:
        json.dump({"data": unique_hotels}, f, indent=2)

def update_hotels(hotels):
    for hotel in hotels:
        if hotel.get("website") is None:
            hotel["website"] = "https://www.hotels.com/Hotel-Search?latLong=%f,%f&sort=DISTANCE" % (hotel["lat"], hotel["long"])
    return hotels


def merge_data(old_data, new_data):
    # Turn array of dicts into dict of dicts to make merging faster
    #  key has to be unique per entry, so use "lat + long + name" string
    old_data_dict = {str(d["lat"] + d["long"]) + d["name"] : d for d in old_data}
    new_data_dict = {str(d["lat"] + d["long"]) + d["name"] : d for d in new_data}

    # Check each point in old data for updated values
    for latlongname, old_d in old_data_dict.items():
        if latlongname not in new_data_dict:
            continue
        for key, value in new_data_dict[latlongname].items():
            if key == "id":
                continue
            if key not in old_d or old_d.get(key) != value:
                old_d[key] = value

    return list(old_data_dict.values())



def pick_n_nearest(instance, options, n):
    def option_dist(option):
        instance_point = (instance["lat"], instance["long"])
        option_point = (option["lat"], option["long"])
        option_dist = {
            "id": option["id"],
            "name": option["name"],
            "dist": geopy.distance.distance(instance_point, option_point).mi
        }
        return option_dist
    options_map = map(option_dist, options)
    nearest = sorted(options_map, key=lambda d: d['dist'])
    return nearest[:n]
        


def main():
    # Scrape data
    """
    print("Starting national parks scrape...")
    national_parks = scrape_national_parks()
    with open("nationalparks.json", "w") as f:
        json.dump({"data": national_parks}, f, indent=2)

    print("Starting airports scrape...")
    airports = scrape_airports()
    with open("airports.json", "w") as f:
        json.dump({"data": airports}, f, indent=2)

    # NOTE: Just looking for nearby to parks right now
    print("Starting hotels scrape...")
    hotels = scrape_hotels(national_parks.get("data"))
    with open("hotels.json", "w") as f:
        json.dump({"data": hotels}, f, indent=2)
    """


    # Update existing data with new/updated fields
    """
    print("Starting national parks update...")
    updated_national_parks = scrape_national_parks()
    with open("nationalparks.json", "r") as f:
        national_parks = json.load(f)
    national_parks = national_parks.get("data")
    merged_national_parks = merge_data(national_parks, updated_national_parks)
    assert len(national_parks) == len(merged_national_parks)
    with open("nationalparks.json", "w") as f:
        json.dump({"data": merged_national_parks}, f, indent=2)

    print("Starting airports update...")
    with open("airports.json", "r") as f:
        airports = json.load(f)
    airports = airports.get("data")
    merged_airports = update_airports(airports)
    assert len(airports) == len(merged_airports)
    with open("airports.json", "w") as f:
        json.dump({"data": merged_airports}, f, indent=2)

    print("Starting hotels update...")
    with open("hotels.json", "r") as f:
        hotels = json.load(f)
    hotels = hotels.get("data")
    merged_hotels = update_hotels(hotels)
    assert len(hotels) == len(merged_hotels)
    with open("hotels.json", "w") as f:
        json.dump({"data": merged_hotels}, f, indent=2)
    """


    # Add "nearest" fields to each dataset
    """
    with open("nationalparks.json", "r") as f:
        national_parks = json.load(f)
    national_parks = national_parks.get("data")
    with open("airports.json", "r") as f:
        airports = json.load(f)
    airports = airports.get("data")
    with open("hotels.json", "r") as f:
        hotels = json.load(f)
    hotels = hotels.get("data")

    print("Starting national parks nearest...")
    for np in national_parks:
        if np["id"] % 10 == 0:
            print("Completed %d national parks" % np["id"])
        np["nearest_airports"] = pick_n_nearest(np, airports, 3)
        np["nearest_hotels"] = pick_n_nearest(np, hotels, 3)
    with open("nationalparks.json", "w") as f:
        json.dump({"data": national_parks}, f, indent=2)

    print("Starting airports nearest...")
    for a in airports:
        if a["id"] % 1000 == 0:
            print("Completed %d airports" % a["id"])
        a["nearest_national_parks"] = pick_n_nearest(a, national_parks, 3)
        a["nearest_hotels"] = pick_n_nearest(a, hotels, 3)
    with open("airports.json", "w") as f:
        json.dump({"data": airports}, f, indent=2)

    print("Starting hotels nearest...")
    for h in hotels:
        if h["id"] % 1000 == 0:
            print("Completed %d hotels" % h["id"])
        h["nearest_airports"] = pick_n_nearest(h, airports, 3)
        h["nearest_national_parks"] = pick_n_nearest(h, national_parks, 3)
    with open("hotels.json", "w") as f:
        json.dump({"data": hotels}, f, indent=2)
    """


if __name__ == "__main__":
    main()
