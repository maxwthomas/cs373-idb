"""
Based off of implementation in Catching Congress IDB Project

Link: https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/database/db_setup.py
"""

import json
from os import getenv

import mysql.connector
from dotenv import load_dotenv

load_dotenv()


# Returns connected database instance (This will always have admin access)
def db_connect():
    return mysql.connector.connect(
        host = getenv("DATABASE_HOST"),
        user = getenv("DATABASE_USER"),
        password = getenv("DATABASE_PASSWORD"),
        database = getenv("DATABASE_NAME")
    )


# Creates database if it does not already exist
def create_db(connection):
    database = getenv("DATABASE_NAME")

    with connection.cursor() as cursor:
        cursor.execute(f"CREATE DATABASE IF NOT EXISTS {database}")
        connection.commit()


# Creates tables to store JSON data scraped from APIs
def create_tables(connection):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS airports(
                id INT NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL,
                type VARCHAR(255) NOT NULL,
                latitude FLOAT NOT NULL,
                longitude FLOAT NOT NULL,
                elevation INT,
                municipality VARCHAR(255),
                has_schedule_service BOOL NOT NULL,
                icao VARCHAR(255) NOT NULL,
                iata VARCHAR(255),
                website VARCHAR(255),
                website_wiki VARCHAR(255),
                state VARCHAR(255) NOT NULL,
                zip INT,
                nearest_hotels JSON,
                nearest_national_parks JSON,
                PRIMARY KEY (id)
            )
            """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS hotels(
                id INT NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL,
                type VARCHAR(255) NOT NULL,
                latitude FLOAT NOT NULL,
                longitude FLOAT NOT NULL,
                addr VARCHAR(255),
                city VARCHAR(255),
                county VARCHAR(255),
                state VARCHAR(255),
                zip INT,
                brand VARCHAR(255),
                website VARCHAR(255),
                website_wiki VARCHAR(255),
                timezone VARCHAR(255),
                nearest_airports JSON,
                nearest_national_parks JSON,
                PRIMARY KEY (id)
            )
            """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS national_parks(
                id INT NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL,
                designation VARCHAR(255) NOT NULL,
                description TEXT NOT NULL,
                latitude FLOAT NOT NULL,
                longitude FLOAT NOT NULL,
                weather TEXT NOT NULL,
                website VARCHAR(255),
                activities TEXT NOT NULL,
                topics TEXT NOT NULL,
                state VARCHAR(255) NOT NULL,
                city VARCHAR(255),
                zip INT,
                addr VARCHAR(255),
                monday VARCHAR(255),
                tuesday VARCHAR(255),
                wednesday VARCHAR(255),
                thursday VARCHAR(255),
                friday VARCHAR(255),
                saturday VARCHAR(255),
                sunday VARCHAR(255),
                price INT,
                images JSON,
                nearest_airports JSON,
                nearest_hotels JSON,
                PRIMARY KEY (id)
            )
            """
        )

        connection.commit()


# Fill tables with JSON data from respective files
def fill_tables(connection):
    # File environment variables
    airport_file = getenv("AIRPORT_FILE")
    hotel_file = getenv("HOTEL_FILE")
    national_park_file = getenv("NATIONAL_PARK_FILE")

    # JSON variables
    data = getenv("DATA")
    images = getenv("IMAGES")
    nearest_airports = getenv("NEAREST_AIRPORTS")
    nearest_hotels = getenv("NEAREST_HOTELS")
    nearest_parks = getenv("NEAREST_PARKS")

    with connection.cursor() as cursor:
        with open(airport_file, encoding="utf-8") as file:
            airports = json.load(file)[data]

        for airport in airports:
            airport[nearest_hotels] = json.dumps(airport[nearest_hotels])
            airport[nearest_parks] = json.dumps(airport[nearest_parks])
            try:
                cursor.execute(
                    """
                    INSERT INTO airports(
                        id,
                        name,
                        type,
                        latitude,
                        longitude,
                        elevation,
                        municipality,
                        has_schedule_service,
                        icao,
                        iata,
                        website,
                        website_wiki,
                        state,
                        zip,
                        nearest_hotels,
                        nearest_national_parks
                    )
                    VALUES(
                        %(id)s,
                        %(name)s,
                        %(type)s,
                        %(lat)s,
                        %(long)s,
                        %(elevation)s,
                        %(municipality)s,
                        %(has_schedule_service)s,
                        %(icao)s,
                        %(iata)s,
                        %(website)s,
                        %(website_wiki)s,
                        %(state)s,
                        %(zip)s,
                        %(nearest_hotels)s,
                        %(nearest_national_parks)s
                    )
                    """,
                    airport
                )
            except Exception as e:
                print("FAILED TO UPLOAD AIRPORT:")
                print(airport)
                print(e)
        
        with open(hotel_file, encoding="utf-8") as file:
            hotels = json.load(file)[data]

        for hotel in hotels:
            hotel[nearest_airports] = json.dumps(hotel[nearest_airports])
            hotel[nearest_parks] = json.dumps(hotel[nearest_parks]) 
            try:
                cursor.execute(
                    """
                    INSERT INTO hotels(
                        id,
                        name,
                        latitude,
                        longitude,
                        addr,
                        city,
                        county,
                        state,
                        zip,
                        brand,
                        website,
                        website_wiki,
                        timezone,
                        nearest_airports,
                        nearest_national_parks
                    )
                    VALUES(
                        %(id)s,
                        %(name)s,
                        %(lat)s,
                        %(long)s,
                        %(addr)s,
                        %(city)s,
                        %(county)s,
                        %(state)s,
                        %(zip)s,
                        %(brand)s,
                        %(website)s,
                        %(website_wiki)s,
                        %(timezone)s,
                        %(nearest_airports)s,
                        %(nearest_national_parks)s
                    )
                    """,
                    hotel
                )
            except Exception as e:
                print("FAILED TO UPLOAD HOTEL:")
                print(hotel)
                print(e)

        with open(national_park_file, encoding="utf-8") as file:
            national_parks = json.load(file)[data]

        for park in national_parks:
            park[images] = json.dumps(park[images])
            park[nearest_airports] = json.dumps(park[nearest_airports])
            park[nearest_hotels] = json.dumps(park[nearest_hotels])
       
            try:
                cursor.execute(
                    """
                    INSERT INTO national_parks(
                        id,
                        name,
                        designation,
                        description,
                        latitude,
                        longitude,
                        weather,
                        website,
                        activities,
                        topics,
                        state,
                        city,
                        zip,
                        addr,
                        monday,
                        tuesday,
                        wednesday,
                        thursday,
                        friday,
                        saturday,
                        sunday,
                        price,
                        images,
                        nearest_airports,
                        nearest_hotels
                    )
                    VALUES(
                        %(id)s,
                        %(name)s,
                        %(designation)s,
                        %(description)s,
                        %(lat)s,
                        %(long)s,
                        %(weather)s,
                        %(website)s,
                        %(activities)s,
                        %(topics)s,
                        %(state)s,
                        %(city)s,
                        %(zip)s,
                        %(addr)s,
                        %(monday)s,
                        %(tuesday)s,
                        %(wednesday)s,
                        %(thursday)s,
                        %(friday)s,
                        %(saturday)s,
                        %(sunday)s,
                        %(price)s,
                        %(images)s,
                        %(nearest_airports)s,
                        %(nearest_hotels)s
                    )
                    """,
                    park
                )
            except Exception as e:
                print("FAILED TO UPLOAD PARK:")
                print(park)
                print(e)

        connection.commit()


# Delete tables if they have been created already
def drop_tables(connection):
    with connection.cursor() as cursor:
        cursor.execute("DROP TABLE IF EXISTS airports")
        cursor.execute("DROP TABLE IF EXISTS hotels")
        cursor.execute("DROP TABLE IF EXISTS national_parks")
        connection.commit()


# Main Function
def main():
    # Get connection
    c = db_connect()

    # Create the database
    create_db(c)

    # Manage tables
    drop_tables(c)
    print("dropped previous tables.")
    create_tables(c)
    print("created new tables.")
    fill_tables(c)
    print("filled new tables.")

    print("done.")


if __name__ == "__main__":
    main()
