from sqlalchemy import and_, or_, Integer
from sqlalchemy.sql.expression import desc
from stopwords import stopwords
from flask import jsonify


def to_dict(obj):
    return {c.name: getattr(obj, c.name) for c in obj.__table__.columns}


def deserialize_arr(str):
    res = str.split('","')
    res[0] = res[0][1:]
    res[-1] = res[-1][:-1]
    return res


def format_park(park):
    park["activities"] = deserialize_arr(park["activities"])
    park["topics"] = deserialize_arr(park["topics"])
    park["hours"] = {}
    for day in (
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "sunday",
    ):
        park["hours"][day] = park[day]
        del park[day]
    return park


def parse_search(query):
    if not query:
        return []
    res = query.split()
    return [word for word in res if word.lower() not in stopwords]


def query_model_multiple(Model, args, format=lambda d: d):
    # filtering
    filters = {key: val for key, val in args.items() if key in Model.filters}
    filter_exprs = [getattr(Model, att) == val for att, val in filters.items()]
    list_filters = {
        Model.list_filters[key]: val
        for key, val in args.items()
        if key in Model.list_filters
    }
    list_filter_exprs = [
        getattr(Model, att).ilike("%" + val + "%") for att, val in list_filters.items()
    ]
    has_filters = {
        key[4:]: val
        for key, val in args.items()
        if key[:4] == "has_" and key[4:] in Model.has_filters
    }
    has_filter_exprs = [
        getattr(Model, att).isnot(None)
        if int(val) > 0
        else getattr(Model, att).is_(None)
        for att, val in has_filters.items()
    ]
    max_filters = {key: val for key, val in args.items() if key in Model.max_filters}
    max_filter_exprs = [getattr(Model, att) <= val for att, val in filters.items()]
    filter_bool = and_(
        True, *filter_exprs, *list_filter_exprs, *has_filter_exprs, *max_filter_exprs
    )
    res = Model.query.filter(filter_bool)

    # searching
    search_terms = parse_search(args.get("q", default=None))
    if len(search_terms) > 0:
        search_filter_exprs = [
            (
                and_(
                    getattr(Model, att).isnot(None),
                    getattr(Model, att).ilike("%" + term + "%"),
                )
            ).cast(Integer)
            * weight
            for term in search_terms
            for att, weight in Model.search_weights.items()
        ]
        search_weight = sum(search_filter_exprs)
        res = res.filter(search_weight != 0)
        res = res.order_by(desc(search_weight))

    # sorting
    sort_attribute = args.get("sort", default="name")
    sort_expr = getattr(Model, sort_attribute)
    sort_desc = args.get("desc", default=False)
    res = res.order_by(desc(sort_expr) if sort_desc else sort_expr)

    page = args.get("page", default=1, type=int)
    res = res.paginate(page=page, per_page=Model.items_per_page)
    return jsonify(
        {
            "count": res.total,
            "page": page,
            "total_pages": res.pages,
            "data": [format(to_dict(item)) for item in res.items],
            "search_terms": search_terms,
        }
    )


def query_model_single(Model, id, format=lambda d: d):
    res = Model.query.filter_by(id=id).first()
    return jsonify(format(to_dict(res)))
