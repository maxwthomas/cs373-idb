# CS-373 IDB: Parkdex

##### Table of Contents  
- [About](#about)
	- [Frontend](#frontend)
	- [Backend](#backend)
	- [DevOps](#devops)
	- [Cloud](#Cloud)
- [Phases](#phases)
	- [Phase 1](#phase-1)
	- [Phase 2](#phase-2)
	- [Phase 3](#phase-3)
	- [Phase 4](#phase-4)
- [Proposal](#proposal)  


## About
**Members**:
| Name            | EID     | GitLab ID       |
| --------------- | ------- | --------------- |
| Emin Arslan     | eaa3228 | @eminalparslan  |
| Manish Bhandari | mb59875 | @manishbh       |
| Max Thomas      | mwt568  | @maxwthomas     |
| Justin Xie      | jyx69   | @celebi1023     |
| Lawrence Zhang  | llz257  | @lawrencezhang1 |

**Website**: https://www.parkdex.me

**GitLab Pipeline**: https://gitlab.com/maxwthomas/cs373-idb/-/pipelines

### Frontend
See [here](frontend/README.md)

### Backend
See [here](backend/README.md)

### DevOps

#### Makefiles
Each directory contains the following targets:
1. `make docker-install` - creates the docker image which installs all the necessary dependencies to run a development instance in a docker image
2. `make docker-run` - runs the development instance using the docker image
3. `make install` - install the necessary packages to run a development instance
4. `make run` - runs the development instance
5. `make test` - runs all available tests
6. `make unit-test` - runs all available unit tests
7. `make acceptance-test` - runs all available acceptance tests
8. `make format` - formats the code
9. `make format-check` - verifys that the code is correctly formatted
10. `make build` - produces files for a production instance
11. `make clean` - removes any artifacts needed to run a development instance

#### Pipelines
A very basic pipeline has been setup for the frontend and backend to simply install and build an instance of each one
Phases:
1. build
	- build-frontend
	- build-backend

### Cloud

#### AWS Amplify and Namecheap
Our website and domain are hosted through Amazon Web Services and Namecheap. Specifically, we have our frontend built using AWS Amplify through GitLab. Amplify allows us to deploy with continuous integration on different branches during development and display selected branches on our host domain.

What AWS Amplify does:
1. Connects to given GitLab repository and selects branches to build and deploy
2. Scrapes files to find build commands to run for web application (stored in `.gitlab-ci.yml` file)
3. Hosts application on a selected branch at pretty URL from the Namecheap acquired domain with provided HTTPS certification
2. Rebuilds and deploys to target domain each time new commits are made

For more information, you can visit the AWS Amplify Hosting User Guide [here](https://docs.aws.amazon.com/amplify/latest/userguide/getting-started.html).

#### AWS Relational Database Service
Our cloud database is built through AWS RDS, a scalable relational database service. It uses MySQL for its engine and runs quickly and securely through data encryption and SSD-backed storage.

In our application, we can populate this database through the `backend/db_init.py` script, which specifically targets the `.json` files created from our scraper and builds relational tables through them to represent each of our data models. Environment variables are accessed and stored locally for security.

#### AWS Elastic Beanstalk
We have our API built through Flask and Python through a MySQL database. The base of our Flask app is stored at `/backend/app.py`. We then use nginx and uWSGI as a reverse proxy for handling connections and traffic and a full stack application server for deploying our app respectively. Configurations for these two processes can be found in `/backend/nginx.conf` and `uwsgi.ini`, and are launched through the `start.sh` script.

AWS Elastic Beanstalk is a platform that enables web application deployment. The process is as follows:
1. Through the Elastic Beanstalk Command Line Interface (CLI), we initialize our application with the Docker image specified in `/backend` using our AWS credentials\
	&emsp;`eb init -p docker parkdex-backend`
2. We are then able to get our Flask app and Docker image deployed onto AWS\
	&emsp;`eb create parkdex-api-prod`
3. With CI/CD, each time we push changes to our applicatin, our `.gitlab-ci.yml` configuration will then redeploy our API instance through Elastic Beanstalk and make it accessible through a URL

We then have Namecheap redirect the `api` subdomain for our app to this Elastic Beanstalk instance, allowing our API to be openly accessible through the `https://api.parkdex.me` endpoint.

## Phases

### Phase 1
**Phase Leader**: Max Thomas

**Estimated Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 8     |
| Manish Bhandari | 10.5  |
| Max Thomas      | 7     |
| Justin Xie      | 6.5   |
| Lawrence Zhang  | 6.75  |

**Actual Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 8     |
| Manish Bhandari | 10.92 |
| Max Thomas      | 10.75 |
| Justin Xie      | 4.75  |
| Lawrence Zhang  | 9.25  |

**Git SHA**: bfae91526d9d30354c5f4319fc838d1c2bb25e9a

**Comments**: N/A

### Phase 2
**Phase Leader**: Emin Arslan

**Estimated Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 10.5  |
| Manish Bhandari | 7     |
| Max Thomas      | 8     |
| Justin Xie      | 10    |
| Lawrence Zhang  | 7     |

**Actual Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 12    |
| Manish Bhandari | 9     |
| Max Thomas      | 15.5  |
| Justin Xie      | 7.5   |
| Lawrence Zhang  | 23.75 |

**Git SHA**: 23650041adc2ddda095ac58e14edd295de8cdd36

**Comments**: 1 slip day used.

### Phase 3
**Phase Leader**: Justin Xie

**Estimated Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 8.25  |
| Manish Bhandari | 10    |
| Max Thomas      | 7     |
| Justin Xie      | 4     |
| Lawrence Zhang  | 5     |

**Actual Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 6.25  |
| Manish Bhandari | 7     |
| Max Thomas      | 11.5  |
| Justin Xie      | 10    |
| Lawrence Zhang  | 5.25  |

**Git SHA**: ade03af4f582250c0f92df9d10fd28f0be7e6d5a

**Comments**: N/A

### Phase 4
**Phase Leader**: Lawrence Zhang

**Estimated Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 4.5   |
| Manish Bhandari | 3     |
| Max Thomas      | 3.5   |
| Justin Xie      | 3     |
| Lawrence Zhang  | 3     |

**Actual Completition Time**:
| Name            | Hours |
| --------------- | ----- |
| Emin Arslan     | 4.5   |
| Manish Bhandari | 2     |
| Max Thomas      | 4     |
| Justin Xie      | 2     |
| Lawrence Zhang  | 5     |

**Git SHA**: 271cff12e22299f8bd0e63ce5508ba06cf487a7d

**Comments**: N/A


## Proposal
**Group**: IDB Group 11-6

**Members**: Emin Arslan, Manish Bhandari, Max Thomas, Justin Xie, Lawrence Zhang

**Project Name**: Parkdex

**URL**: https://gitlab.com/maxwthomas/cs373-idb

**Description**: A webpage that displays all of the national parks, airports, and hotels in the United States with the goal of letting a user find a national park they would like to visit and then find nearby transportation and lodging to make that visit possible. The national parks are a part of the US government services and are great display of natural beauty and this site would encorage users to go visit!

**Data Sources**:
1. https://www.nps.gov/subjects/developer/api-documentation.htm#/parks/getPark
2. https://rapidapi.com/sujayvsarma/api/ourairport-data-search
3. https://apidocs.geoapify.com/docs/places/#about and https://apidocs.geoapify.com/docs/place-details/#place-details

**Models**:
1. National Parks (~400)
2. Airports (~20,000)
3. Hotels (~50,000)

**Attributes**:
1. National Parks
- Filterable: activities, state, cost, topics, designation (eg, national park, national monument, national recreation area, etc)
- Searchable: name, weather, zip code, city, open days
- Connections: nearest airport, neareast hotel
- Media: map (lat/long), image of park
1. Airports
- Filterable: state, type, has website, has scheduling service, has wiki page
- Searchable: name, municipality, icao code, iata code, zip code
- Connections: nearest national park, nearest hotel
- Media: map (lat/long), website
1. Hotels
- Filterable: state, county, brand, has website, has wiki page
- Searchable: name, zip code, city, address, timezone
- Connections: nearest national park, nearest hotel
- Media: map (lat/long), website

**Questions**:
1. Where is the nearest national park to us in Austin?
2. How expensive is it to visit an average national park?
3. How easy is it to visit a national park? (How close are the nearest accommodations/transportation)
